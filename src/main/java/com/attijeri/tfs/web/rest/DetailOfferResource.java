package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.service.DetailOfferService;
import com.attijeri.tfs.web.rest.errors.BadRequestAlertException;
import com.attijeri.tfs.service.dto.DetailOfferDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.attijeri.tfs.domain.DetailOffer}.
 */
@RestController
@RequestMapping("/api")
public class DetailOfferResource {

    private final Logger log = LoggerFactory.getLogger(DetailOfferResource.class);

    private final DetailOfferService detailOfferService;

    public DetailOfferResource(DetailOfferService detailOfferService) {
        this.detailOfferService = detailOfferService;
    }

    /**
     * {@code GET  /detail-offers} : get all the detailOffers.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of detailOffers in body.
     */
    @GetMapping("/detail-offers")
    public List<DetailOfferDTO> getAllDetailOffers() {
        log.debug("REST request to get all DetailOffers");
        return detailOfferService.findAll();
    }

    /**
     * {@code GET  /detail-offers/:id} : get the "id" detailOffer.
     *
     * @param id the id of the detailOfferDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the detailOfferDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/detail-offers/{id}")
    public ResponseEntity<DetailOfferDTO> getDetailOffer(@PathVariable Long id) {
        log.debug("REST request to get DetailOffer : {}", id);
        Optional<DetailOfferDTO> detailOfferDTO = detailOfferService.findOne(id);
        return ResponseUtil.wrapOrNotFound(detailOfferDTO);
    }
}
