package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.service.RequiredDocResidencyService;
import com.attijeri.tfs.web.rest.errors.BadRequestAlertException;
import com.attijeri.tfs.service.dto.RequiredDocResidencyDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.attijeri.tfs.domain.RequiredDocResidency}.
 */
@RestController
@RequestMapping("/api")
public class RequiredDocResidencyResource {

    private final Logger log = LoggerFactory.getLogger(RequiredDocResidencyResource.class);

    private final RequiredDocResidencyService requiredDocResidencyService;

    public RequiredDocResidencyResource(RequiredDocResidencyService requiredDocResidencyService) {
        this.requiredDocResidencyService = requiredDocResidencyService;
    }

    /**
     * {@code GET  /required-doc-residencies} : get all the requiredDocResidencies.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of requiredDocResidencies in body.
     */
    @GetMapping("/required-doc-residencies")
    public List<RequiredDocResidencyDTO> getAllRequiredDocResidencies() {
        log.debug("REST request to get all RequiredDocResidencies");
        return requiredDocResidencyService.findAll();
    }

    /**
     * {@code GET  /required-doc-residencies/:id} : get the "id" requiredDocResidency.
     *
     * @param id the id of the requiredDocResidencyDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the requiredDocResidencyDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/required-doc-residencies/{id}")
    public ResponseEntity<RequiredDocResidencyDTO> getRequiredDocResidency(@PathVariable Long id) {
        log.debug("REST request to get RequiredDocResidency : {}", id);
        Optional<RequiredDocResidencyDTO> requiredDocResidencyDTO = requiredDocResidencyService.findOne(id);
        return ResponseUtil.wrapOrNotFound(requiredDocResidencyDTO);
    }
}
