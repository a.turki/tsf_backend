package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.config.optConfig.MiddleWareService;
import com.attijeri.tfs.service.PersonalInfoService;
import com.attijeri.tfs.service.RequestService;
import com.attijeri.tfs.service.dto.RequestDTO;
import com.attijeri.tfs.web.rest.errors.BadRequestAlertException;
import com.attijeri.tfs.service.dto.PersonalInfoDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.attijeri.tfs.domain.PersonalInfo}.
 */
@RestController
@RequestMapping("/api")
public class PersonalInfoResource {
    private static final String ENTITY_NAME = "personalinfo";
    private final Logger log = LoggerFactory.getLogger(PersonalInfoResource.class);
    private final MiddleWareService middleWareService;
    private final RequestService requestService;
    private final PersonalInfoService personalInfoService;
    @Value("${jhipster.clientApp.name}")
    private String applicationName;
    public PersonalInfoResource(PersonalInfoService personalInfoService,MiddleWareService middleWareService,RequestService requestService) {
        this.personalInfoService = personalInfoService;
        this.middleWareService = middleWareService;
        this.requestService = requestService;
    }

    /**
     * {@code GET  /personal-infos} : get all the personalInfos.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of personalInfos in body.
     */
    @GetMapping("/personal-infos")
    public List<PersonalInfoDTO> getAllPersonalInfos() {
        log.debug("REST request to get all PersonalInfos");
        return personalInfoService.findAll();
    }

    /**
     * {@code GET  /personal-infos/:id} : get the "id" personalInfo.
     *
     * @param id the id of the personalInfoDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the personalInfoDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/personal-infos/{id}")
    public ResponseEntity<PersonalInfoDTO> getPersonalInfo(@PathVariable Long id) {
        log.debug("REST request to get PersonalInfo : {}", id);
        Optional<PersonalInfoDTO> personalInfoDTO = personalInfoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(personalInfoDTO);
    }

    @GetMapping("/personal")
    public void sendMail(){
       // middleWareService.sendConfirmationEmail("Confirmation du mail");
    }

    @PostMapping("/addNewRequest/{idOffer}")
    public ResponseEntity<PersonalInfoDTO> addNewRequest(@RequestBody PersonalInfoDTO personalInfoDTO,@PathVariable Long idOffer) throws URISyntaxException {
        RequestDTO requestDTO = new RequestDTO();
        requestDTO.setOfferId(idOffer);
        requestDTO.setSendingMailDate(LocalDate.now());
        requestDTO.setState(false);
        RequestDTO dto = new RequestDTO();
        PersonalInfoDTO personalInfoDTO1 = new PersonalInfoDTO();
        PersonalInfoDTO personalInfoDTO2 = new PersonalInfoDTO();


        personalInfoDTO1.setCivility(personalInfoDTO.getCivility());
        personalInfoDTO1.setFirstName(personalInfoDTO.getFirstName());
        personalInfoDTO1.setLastName(personalInfoDTO.getLastName());
        personalInfoDTO1.setEmail(personalInfoDTO.getEmail());
        personalInfoDTO1.setPhone(personalInfoDTO.getPhone());
        personalInfoDTO1.setNativeCountry(personalInfoDTO.getNativeCountry());
        personalInfoDTO1.setBirthday(personalInfoDTO.getBirthday());
        if(personalInfoService.validEmail(personalInfoDTO1.getEmail()) == false){
             dto = requestService.save(requestDTO);
            personalInfoDTO1.setRequestId(dto.getId());
           personalInfoDTO2 = personalInfoService.save(personalInfoDTO1);
           middleWareService.sendConfirmationEmail(personalInfoDTO2,"Validation du mail");
        }
        return ResponseEntity.created(new URI("/api/addNewRequest/" + personalInfoDTO2.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, personalInfoDTO2.getId().toString()))
            .body(personalInfoDTO2);
    }

    @PutMapping("/updatePersonalInfo")
    public ResponseEntity<PersonalInfoDTO> update(@RequestBody PersonalInfoDTO personalInfo) {
        log.debug("REST request to update Personal Info : {}", personalInfo.getId());

        PersonalInfoDTO savedDTO = personalInfoService.update(personalInfo);
        if (savedDTO != null) {
            return new ResponseEntity<>(savedDTO, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
   /* @GetMapping("/requests/{id}")
    public ResponseEntity<Integer> saveCivility(@PathVariable Long id) {
        log.debug("REST request to get Request : {}", id);
        Optional<RequestDTO> requestDTO = requestService.findOne(id);
        return ResponseUtil.wrapOrNotFound(requestDTO);
    }*/
}
