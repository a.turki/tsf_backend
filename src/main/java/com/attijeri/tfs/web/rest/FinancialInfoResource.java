package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.service.FinancialInfoService;
import com.attijeri.tfs.web.rest.errors.BadRequestAlertException;
import com.attijeri.tfs.service.dto.FinancialInfoDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.attijeri.tfs.domain.FinancialInfo}.
 */
@RestController
@RequestMapping("/api")
public class FinancialInfoResource {

    private final Logger log = LoggerFactory.getLogger(FinancialInfoResource.class);

    private final FinancialInfoService financialInfoService;

    public FinancialInfoResource(FinancialInfoService financialInfoService) {
        this.financialInfoService = financialInfoService;
    }

    /**
     * {@code GET  /financial-infos} : get all the financialInfos.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of financialInfos in body.
     */
    @GetMapping("/financial-infos")
    public List<FinancialInfoDTO> getAllFinancialInfos() {
        log.debug("REST request to get all FinancialInfos");
        return financialInfoService.findAll();
    }

    /**
     * {@code GET  /financial-infos/:id} : get the "id" financialInfo.
     *
     * @param id the id of the financialInfoDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the financialInfoDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/financial-infos/{id}")
    public ResponseEntity<FinancialInfoDTO> getFinancialInfo(@PathVariable Long id) {
        log.debug("REST request to get FinancialInfo : {}", id);
        Optional<FinancialInfoDTO> financialInfoDTO = financialInfoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(financialInfoDTO);
    }
}
