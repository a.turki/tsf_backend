package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.service.AddressInfoService;
import com.attijeri.tfs.web.rest.errors.BadRequestAlertException;
import com.attijeri.tfs.service.dto.AddressInfoDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

/**
 * REST controller for managing {@link com.attijeri.tfs.domain.AddressInfo}.
 */
@RestController
@RequestMapping("/api")
public class AddressInfoResource {

    private final Logger log = LoggerFactory.getLogger(AddressInfoResource.class);

    private final AddressInfoService addressInfoService;

    public AddressInfoResource(AddressInfoService addressInfoService) {
        this.addressInfoService = addressInfoService;
    }

    /**
     * {@code GET  /address-infos} : get all the addressInfos.
     *
     * @param filter the filter of the request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of addressInfos in body.
     */
    @GetMapping("/address-infos")
    public List<AddressInfoDTO> getAllAddressInfos(@RequestParam(required = false) String filter) {
        if ("personalinfo-is-null".equals(filter)) {
            log.debug("REST request to get all AddressInfos where personalInfo is null");
            return addressInfoService.findAllWherePersonalInfoIsNull();
        }
        log.debug("REST request to get all AddressInfos");
        return addressInfoService.findAll();
    }

    /**
     * {@code GET  /address-infos/:id} : get the "id" addressInfo.
     *
     * @param id the id of the addressInfoDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the addressInfoDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/address-infos/{id}")
    public ResponseEntity<AddressInfoDTO> getAddressInfo(@PathVariable Long id) {
        log.debug("REST request to get AddressInfo : {}", id);
        Optional<AddressInfoDTO> addressInfoDTO = addressInfoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(addressInfoDTO);
    }
}
