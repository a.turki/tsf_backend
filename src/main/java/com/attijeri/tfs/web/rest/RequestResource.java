package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.service.RequestService;
import com.attijeri.tfs.web.rest.errors.BadRequestAlertException;
import com.attijeri.tfs.service.dto.RequestDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

/**
 * REST controller for managing {@link com.attijeri.tfs.domain.Request}.
 */
@RestController
@RequestMapping("/api")
public class RequestResource {

    private final Logger log = LoggerFactory.getLogger(RequestResource.class);

    private final RequestService requestService;

    public RequestResource(RequestService requestService) {
        this.requestService = requestService;
    }

    /**
     * {@code GET  /requests} : get all the requests.
     *
     * @param filter the filter of the request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of requests in body.
     */
    @GetMapping("/requests")
    public List<RequestDTO> getAllRequests(@RequestParam(required = false) String filter) {
        if ("personalinfo-is-null".equals(filter)) {
            log.debug("REST request to get all Requests where personalInfo is null");
            return requestService.findAllWherePersonalInfoIsNull();
        }
        if ("financialinfo-is-null".equals(filter)) {
            log.debug("REST request to get all Requests where financialInfo is null");
            return requestService.findAllWhereFinancialInfoIsNull();
        }
        if ("requireddoc-is-null".equals(filter)) {
            log.debug("REST request to get all Requests where requiredDoc is null");
            return requestService.findAllWhereRequiredDocIsNull();
        }
        log.debug("REST request to get all Requests");
        return requestService.findAll();
    }

    /**
     * {@code GET  /requests/:id} : get the "id" request.
     *
     * @param id the id of the requestDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the requestDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/requests/{id}")
    public ResponseEntity<RequestDTO> getRequest(@PathVariable Long id) {
        log.debug("REST request to get Request : {}", id);
        Optional<RequestDTO> requestDTO = requestService.findOne(id);
        return ResponseUtil.wrapOrNotFound(requestDTO);
    }
}
