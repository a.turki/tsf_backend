package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.service.RequiredDocService;
import com.attijeri.tfs.web.rest.errors.BadRequestAlertException;
import com.attijeri.tfs.service.dto.RequiredDocDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.attijeri.tfs.domain.RequiredDoc}.
 */
@RestController
@RequestMapping("/api")
public class RequiredDocResource {

    private final Logger log = LoggerFactory.getLogger(RequiredDocResource.class);

    private final RequiredDocService requiredDocService;

    public RequiredDocResource(RequiredDocService requiredDocService) {
        this.requiredDocService = requiredDocService;
    }

    /**
     * {@code GET  /required-docs} : get all the requiredDocs.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of requiredDocs in body.
     */
    @GetMapping("/required-docs")
    public List<RequiredDocDTO> getAllRequiredDocs() {
        log.debug("REST request to get all RequiredDocs");
        return requiredDocService.findAll();
    }

    /**
     * {@code GET  /required-docs/:id} : get the "id" requiredDoc.
     *
     * @param id the id of the requiredDocDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the requiredDocDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/required-docs/{id}")
    public ResponseEntity<RequiredDocDTO> getRequiredDoc(@PathVariable Long id) {
        log.debug("REST request to get RequiredDoc : {}", id);
        Optional<RequiredDocDTO> requiredDocDTO = requiredDocService.findOne(id);
        return ResponseUtil.wrapOrNotFound(requiredDocDTO);
    }
}
