package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.service.FAQService;
import com.attijeri.tfs.web.rest.errors.BadRequestAlertException;
import com.attijeri.tfs.service.dto.FAQDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.attijeri.tfs.domain.FAQ}.
 */
@RestController
@RequestMapping("/api")
public class FAQResource {

    private final Logger log = LoggerFactory.getLogger(FAQResource.class);

    private final FAQService fAQService;

    public FAQResource(FAQService fAQService) {
        this.fAQService = fAQService;
    }

    /**
     * {@code GET  /faqs} : get all the fAQS.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of fAQS in body.
     */
    @GetMapping("/faqs")
    public List<FAQDTO> getAllFAQS() {
        log.debug("REST request to get all FAQS");
        return fAQService.findAll();
    }

    /**
     * {@code GET  /faqs/:id} : get the "id" fAQ.
     *
     * @param id the id of the fAQDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the fAQDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/faqs/{id}")
    public ResponseEntity<FAQDTO> getFAQ(@PathVariable Long id) {
        log.debug("REST request to get FAQ : {}", id);
        Optional<FAQDTO> fAQDTO = fAQService.findOne(id);
        return ResponseUtil.wrapOrNotFound(fAQDTO);
    }
}
