package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.service.RequiredDocIncomeService;
import com.attijeri.tfs.web.rest.errors.BadRequestAlertException;
import com.attijeri.tfs.service.dto.RequiredDocIncomeDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.attijeri.tfs.domain.RequiredDocIncome}.
 */
@RestController
@RequestMapping("/api")
public class RequiredDocIncomeResource {

    private final Logger log = LoggerFactory.getLogger(RequiredDocIncomeResource.class);

    private final RequiredDocIncomeService requiredDocIncomeService;

    public RequiredDocIncomeResource(RequiredDocIncomeService requiredDocIncomeService) {
        this.requiredDocIncomeService = requiredDocIncomeService;
    }

    /**
     * {@code GET  /required-doc-incomes} : get all the requiredDocIncomes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of requiredDocIncomes in body.
     */
    @GetMapping("/required-doc-incomes")
    public List<RequiredDocIncomeDTO> getAllRequiredDocIncomes() {
        log.debug("REST request to get all RequiredDocIncomes");
        return requiredDocIncomeService.findAll();
    }

    /**
     * {@code GET  /required-doc-incomes/:id} : get the "id" requiredDocIncome.
     *
     * @param id the id of the requiredDocIncomeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the requiredDocIncomeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/required-doc-incomes/{id}")
    public ResponseEntity<RequiredDocIncomeDTO> getRequiredDocIncome(@PathVariable Long id) {
        log.debug("REST request to get RequiredDocIncome : {}", id);
        Optional<RequiredDocIncomeDTO> requiredDocIncomeDTO = requiredDocIncomeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(requiredDocIncomeDTO);
    }
}
