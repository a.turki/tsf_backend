package com.attijeri.tfs.service.mapper;


import com.attijeri.tfs.domain.*;
import com.attijeri.tfs.service.dto.DetailOfferDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link DetailOffer} and its DTO {@link DetailOfferDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DetailOfferMapper extends EntityMapper<DetailOfferDTO, DetailOffer> {


    @Mapping(target = "offers", ignore = true)
    @Mapping(target = "removeOffer", ignore = true)
    DetailOffer toEntity(DetailOfferDTO detailOfferDTO);

    default DetailOffer fromId(Long id) {
        if (id == null) {
            return null;
        }
        DetailOffer detailOffer = new DetailOffer();
        detailOffer.setId(id);
        return detailOffer;
    }
}
