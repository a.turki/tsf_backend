package com.attijeri.tfs.service.mapper;


import com.attijeri.tfs.domain.*;
import com.attijeri.tfs.service.dto.RequestDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Request} and its DTO {@link RequestDTO}.
 */
@Mapper(componentModel = "spring", uses = {OfferMapper.class, AgencyMapper.class})
public interface RequestMapper extends EntityMapper<RequestDTO, Request> {

    @Mapping(source = "offer.id", target = "offerId")
    @Mapping(source = "agency.id", target = "agencyId")
    RequestDTO toDto(Request request);

    @Mapping(source = "offerId", target = "offer")
    @Mapping(source = "agencyId", target = "agency")
    @Mapping(target = "personalInfo", ignore = true)
    @Mapping(target = "financialInfo", ignore = true)
    @Mapping(target = "requiredDoc", ignore = true)
    @Mapping(target = "bankAccounts", ignore = true)
    @Mapping(target = "removeBankAccount", ignore = true)
    Request toEntity(RequestDTO requestDTO);

    default Request fromId(Long id) {
        if (id == null) {
            return null;
        }
        Request request = new Request();
        request.setId(id);
        return request;
    }
}
