package com.attijeri.tfs.service.mapper;


import com.attijeri.tfs.domain.*;
import com.attijeri.tfs.service.dto.FinancialInfoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link FinancialInfo} and its DTO {@link FinancialInfoDTO}.
 */
@Mapper(componentModel = "spring", uses = {CategoryMapper.class, ActivityMapper.class, RequestMapper.class})
public interface FinancialInfoMapper extends EntityMapper<FinancialInfoDTO, FinancialInfo> {

    @Mapping(source = "category.id", target = "categoryId")
    @Mapping(source = "activity.id", target = "activityId")
    @Mapping(source = "request.id", target = "requestId")
    FinancialInfoDTO toDto(FinancialInfo financialInfo);

    @Mapping(source = "categoryId", target = "category")
    @Mapping(source = "activityId", target = "activity")
    @Mapping(source = "requestId", target = "request")
    FinancialInfo toEntity(FinancialInfoDTO financialInfoDTO);

    default FinancialInfo fromId(Long id) {
        if (id == null) {
            return null;
        }
        FinancialInfo financialInfo = new FinancialInfo();
        financialInfo.setId(id);
        return financialInfo;
    }
}
