package com.attijeri.tfs.service.mapper;


import com.attijeri.tfs.domain.*;
import com.attijeri.tfs.service.dto.AddressInfoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link AddressInfo} and its DTO {@link AddressInfoDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AddressInfoMapper extends EntityMapper<AddressInfoDTO, AddressInfo> {


    @Mapping(target = "personalInfo", ignore = true)
    AddressInfo toEntity(AddressInfoDTO addressInfoDTO);

    default AddressInfo fromId(Long id) {
        if (id == null) {
            return null;
        }
        AddressInfo addressInfo = new AddressInfo();
        addressInfo.setId(id);
        return addressInfo;
    }
}
