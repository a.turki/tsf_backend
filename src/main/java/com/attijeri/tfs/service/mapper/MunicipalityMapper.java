package com.attijeri.tfs.service.mapper;


import com.attijeri.tfs.domain.*;
import com.attijeri.tfs.service.dto.MunicipalityDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Municipality} and its DTO {@link MunicipalityDTO}.
 */
@Mapper(componentModel = "spring", uses = {GovernorateMapper.class})
public interface MunicipalityMapper extends EntityMapper<MunicipalityDTO, Municipality> {

    @Mapping(source = "governorate.id", target = "governorateId")
    MunicipalityDTO toDto(Municipality municipality);

    @Mapping(target = "agencies", ignore = true)
    @Mapping(target = "removeAgency", ignore = true)
    @Mapping(source = "governorateId", target = "governorate")
    Municipality toEntity(MunicipalityDTO municipalityDTO);

    default Municipality fromId(Long id) {
        if (id == null) {
            return null;
        }
        Municipality municipality = new Municipality();
        municipality.setId(id);
        return municipality;
    }
}
