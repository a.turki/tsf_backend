package com.attijeri.tfs.service.mapper;


import com.attijeri.tfs.domain.*;
import com.attijeri.tfs.service.dto.RequiredDocDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link RequiredDoc} and its DTO {@link RequiredDocDTO}.
 */
@Mapper(componentModel = "spring", uses = {RequestMapper.class})
public interface RequiredDocMapper extends EntityMapper<RequiredDocDTO, RequiredDoc> {

    @Mapping(source = "request.id", target = "requestId")
    RequiredDocDTO toDto(RequiredDoc requiredDoc);

    @Mapping(target = "requiredDocIncomes", ignore = true)
    @Mapping(target = "removeRequiredDocIncome", ignore = true)
    @Mapping(target = "requiredDocResidencies", ignore = true)
    @Mapping(target = "removeRequiredDocResidency", ignore = true)
    @Mapping(source = "requestId", target = "request")
    RequiredDoc toEntity(RequiredDocDTO requiredDocDTO);

    default RequiredDoc fromId(Long id) {
        if (id == null) {
            return null;
        }
        RequiredDoc requiredDoc = new RequiredDoc();
        requiredDoc.setId(id);
        return requiredDoc;
    }
}
