package com.attijeri.tfs.service;

import com.attijeri.tfs.service.dto.RequiredDocIncomeDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.attijeri.tfs.domain.RequiredDocIncome}.
 */
public interface RequiredDocIncomeService {

    /**
     * Save a requiredDocIncome.
     *
     * @param requiredDocIncomeDTO the entity to save.
     * @return the persisted entity.
     */
    RequiredDocIncomeDTO save(RequiredDocIncomeDTO requiredDocIncomeDTO);

    /**
     * Get all the requiredDocIncomes.
     *
     * @return the list of entities.
     */
    List<RequiredDocIncomeDTO> findAll();


    /**
     * Get the "id" requiredDocIncome.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RequiredDocIncomeDTO> findOne(Long id);

    /**
     * Delete the "id" requiredDocIncome.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
