package com.attijeri.tfs.service;

import com.attijeri.tfs.service.dto.AddressInfoDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.attijeri.tfs.domain.AddressInfo}.
 */
public interface AddressInfoService {

    /**
     * Save a addressInfo.
     *
     * @param addressInfoDTO the entity to save.
     * @return the persisted entity.
     */
    AddressInfoDTO save(AddressInfoDTO addressInfoDTO);

    /**
     * Get all the addressInfos.
     *
     * @return the list of entities.
     */
    List<AddressInfoDTO> findAll();
    /**
     * Get all the AddressInfoDTO where PersonalInfo is {@code null}.
     *
     * @return the {@link List} of entities.
     */
    List<AddressInfoDTO> findAllWherePersonalInfoIsNull();


    /**
     * Get the "id" addressInfo.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AddressInfoDTO> findOne(Long id);

    /**
     * Delete the "id" addressInfo.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
