package com.attijeri.tfs.service;

import com.attijeri.tfs.service.dto.MunicipalityDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.attijeri.tfs.domain.Municipality}.
 */
public interface MunicipalityService {

    /**
     * Save a municipality.
     *
     * @param municipalityDTO the entity to save.
     * @return the persisted entity.
     */
    MunicipalityDTO save(MunicipalityDTO municipalityDTO);

    /**
     * Get all the municipalities.
     *
     * @return the list of entities.
     */
    List<MunicipalityDTO> findAll();


    /**
     * Get the "id" municipality.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MunicipalityDTO> findOne(Long id);

    /**
     * Delete the "id" municipality.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
