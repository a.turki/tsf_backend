package com.attijeri.tfs.service.dto;

import java.time.LocalDate;
import java.io.Serializable;

/**
 * A DTO for the {@link com.attijeri.tfs.domain.Request} entity.
 */
public class RequestDTO implements Serializable {

    private Long id;

    private LocalDate visio_date;

    private LocalDate sendingMailDate;

    private Boolean state;


    private Long offerId;

    private Long agencyId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getVisio_date() {
        return visio_date;
    }

    public void setVisio_date(LocalDate visio_date) {
        this.visio_date = visio_date;
    }

    public LocalDate getSendingMailDate() {
        return sendingMailDate;
    }

    public void setSendingMailDate(LocalDate sendingMailDate) {
        this.sendingMailDate = sendingMailDate;
    }

    public Boolean isState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public Long getOfferId() {
        return offerId;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }

    public Long getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(Long agencyId) {
        this.agencyId = agencyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RequestDTO)) {
            return false;
        }

        return id != null && id.equals(((RequestDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RequestDTO{" +
            "id=" + getId() +
            ", visio_date='" + getVisio_date() + "'" +
            ", sendingMailDate='" + getSendingMailDate() + "'" +
            ", state='" + isState() + "'" +
            ", offerId=" + getOfferId() +
            ", agencyId=" + getAgencyId() +
            "}";
    }
}
