package com.attijeri.tfs.service.dto;

import java.time.LocalDate;
import java.io.Serializable;

/**
 * A DTO for the {@link com.attijeri.tfs.domain.RequiredDocResidency} entity.
 */
public class RequiredDocResidencyDTO implements Serializable {
    
    private Long id;

    private String type;

    private String num;

    private LocalDate deliveryDate;

    private LocalDate expirationDate;

    private Boolean illimitedExpirationDate;

    private String path;


    private Long requiredDocId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public LocalDate getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(LocalDate deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Boolean isIllimitedExpirationDate() {
        return illimitedExpirationDate;
    }

    public void setIllimitedExpirationDate(Boolean illimitedExpirationDate) {
        this.illimitedExpirationDate = illimitedExpirationDate;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Long getRequiredDocId() {
        return requiredDocId;
    }

    public void setRequiredDocId(Long requiredDocId) {
        this.requiredDocId = requiredDocId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RequiredDocResidencyDTO)) {
            return false;
        }

        return id != null && id.equals(((RequiredDocResidencyDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RequiredDocResidencyDTO{" +
            "id=" + getId() +
            ", type='" + getType() + "'" +
            ", num='" + getNum() + "'" +
            ", deliveryDate='" + getDeliveryDate() + "'" +
            ", expirationDate='" + getExpirationDate() + "'" +
            ", illimitedExpirationDate='" + isIllimitedExpirationDate() + "'" +
            ", path='" + getPath() + "'" +
            ", requiredDocId=" + getRequiredDocId() +
            "}";
    }
}
