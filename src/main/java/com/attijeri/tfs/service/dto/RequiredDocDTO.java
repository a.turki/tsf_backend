package com.attijeri.tfs.service.dto;

import java.time.LocalDate;
import java.io.Serializable;

/**
 * A DTO for the {@link com.attijeri.tfs.domain.RequiredDoc} entity.
 */
public class RequiredDocDTO implements Serializable {
    
    private Long id;

    private String label;

    private String type;

    private String numCIN;

    private LocalDate deliveryDateCIN;

    private String rectoCIN;

    private String versoCIN;

    private String facta;


    private Long requestId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNumCIN() {
        return numCIN;
    }

    public void setNumCIN(String numCIN) {
        this.numCIN = numCIN;
    }

    public LocalDate getDeliveryDateCIN() {
        return deliveryDateCIN;
    }

    public void setDeliveryDateCIN(LocalDate deliveryDateCIN) {
        this.deliveryDateCIN = deliveryDateCIN;
    }

    public String getRectoCIN() {
        return rectoCIN;
    }

    public void setRectoCIN(String rectoCIN) {
        this.rectoCIN = rectoCIN;
    }

    public String getVersoCIN() {
        return versoCIN;
    }

    public void setVersoCIN(String versoCIN) {
        this.versoCIN = versoCIN;
    }

    public String getFacta() {
        return facta;
    }

    public void setFacta(String facta) {
        this.facta = facta;
    }

    public Long getRequestId() {
        return requestId;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RequiredDocDTO)) {
            return false;
        }

        return id != null && id.equals(((RequiredDocDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RequiredDocDTO{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", type='" + getType() + "'" +
            ", numCIN='" + getNumCIN() + "'" +
            ", deliveryDateCIN='" + getDeliveryDateCIN() + "'" +
            ", rectoCIN='" + getRectoCIN() + "'" +
            ", versoCIN='" + getVersoCIN() + "'" +
            ", facta='" + getFacta() + "'" +
            ", requestId=" + getRequestId() +
            "}";
    }
}
