package com.attijeri.tfs.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.attijeri.tfs.domain.FinancialInfo} entity.
 */
public class FinancialInfoDTO implements Serializable {
    
    private Long id;

    private Double monthlyNetIncome;


    private Long categoryId;

    private Long activityId;

    private Long requestId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getMonthlyNetIncome() {
        return monthlyNetIncome;
    }

    public void setMonthlyNetIncome(Double monthlyNetIncome) {
        this.monthlyNetIncome = monthlyNetIncome;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Long getRequestId() {
        return requestId;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FinancialInfoDTO)) {
            return false;
        }

        return id != null && id.equals(((FinancialInfoDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FinancialInfoDTO{" +
            "id=" + getId() +
            ", monthlyNetIncome=" + getMonthlyNetIncome() +
            ", categoryId=" + getCategoryId() +
            ", activityId=" + getActivityId() +
            ", requestId=" + getRequestId() +
            "}";
    }
}
