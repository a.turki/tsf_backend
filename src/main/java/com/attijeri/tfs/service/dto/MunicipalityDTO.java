package com.attijeri.tfs.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.attijeri.tfs.domain.Municipality} entity.
 */
public class MunicipalityDTO implements Serializable {
    
    private Long id;

    private String name;


    private Long governorateId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getGovernorateId() {
        return governorateId;
    }

    public void setGovernorateId(Long governorateId) {
        this.governorateId = governorateId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MunicipalityDTO)) {
            return false;
        }

        return id != null && id.equals(((MunicipalityDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MunicipalityDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", governorateId=" + getGovernorateId() +
            "}";
    }
}
