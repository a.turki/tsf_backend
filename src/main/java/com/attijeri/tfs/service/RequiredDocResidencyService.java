package com.attijeri.tfs.service;

import com.attijeri.tfs.service.dto.RequiredDocResidencyDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.attijeri.tfs.domain.RequiredDocResidency}.
 */
public interface RequiredDocResidencyService {

    /**
     * Save a requiredDocResidency.
     *
     * @param requiredDocResidencyDTO the entity to save.
     * @return the persisted entity.
     */
    RequiredDocResidencyDTO save(RequiredDocResidencyDTO requiredDocResidencyDTO);

    /**
     * Get all the requiredDocResidencies.
     *
     * @return the list of entities.
     */
    List<RequiredDocResidencyDTO> findAll();


    /**
     * Get the "id" requiredDocResidency.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RequiredDocResidencyDTO> findOne(Long id);

    /**
     * Delete the "id" requiredDocResidency.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
