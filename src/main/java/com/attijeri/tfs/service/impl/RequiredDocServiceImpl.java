package com.attijeri.tfs.service.impl;

import com.attijeri.tfs.service.RequiredDocService;
import com.attijeri.tfs.domain.RequiredDoc;
import com.attijeri.tfs.repository.RequiredDocRepository;
import com.attijeri.tfs.service.dto.RequiredDocDTO;
import com.attijeri.tfs.service.mapper.RequiredDocMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link RequiredDoc}.
 */
@Service
@Transactional
public class RequiredDocServiceImpl implements RequiredDocService {

    private final Logger log = LoggerFactory.getLogger(RequiredDocServiceImpl.class);

    private final RequiredDocRepository requiredDocRepository;

    private final RequiredDocMapper requiredDocMapper;

    public RequiredDocServiceImpl(RequiredDocRepository requiredDocRepository, RequiredDocMapper requiredDocMapper) {
        this.requiredDocRepository = requiredDocRepository;
        this.requiredDocMapper = requiredDocMapper;
    }

    @Override
    public RequiredDocDTO save(RequiredDocDTO requiredDocDTO) {
        log.debug("Request to save RequiredDoc : {}", requiredDocDTO);
        RequiredDoc requiredDoc = requiredDocMapper.toEntity(requiredDocDTO);
        requiredDoc = requiredDocRepository.save(requiredDoc);
        return requiredDocMapper.toDto(requiredDoc);
    }

    @Override
    @Transactional(readOnly = true)
    public List<RequiredDocDTO> findAll() {
        log.debug("Request to get all RequiredDocs");
        return requiredDocRepository.findAll().stream()
            .map(requiredDocMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<RequiredDocDTO> findOne(Long id) {
        log.debug("Request to get RequiredDoc : {}", id);
        return requiredDocRepository.findById(id)
            .map(requiredDocMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete RequiredDoc : {}", id);
        requiredDocRepository.deleteById(id);
    }
}
