package com.attijeri.tfs.service.impl;

import com.attijeri.tfs.service.RequiredDocIncomeService;
import com.attijeri.tfs.domain.RequiredDocIncome;
import com.attijeri.tfs.repository.RequiredDocIncomeRepository;
import com.attijeri.tfs.service.dto.RequiredDocIncomeDTO;
import com.attijeri.tfs.service.mapper.RequiredDocIncomeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link RequiredDocIncome}.
 */
@Service
@Transactional
public class RequiredDocIncomeServiceImpl implements RequiredDocIncomeService {

    private final Logger log = LoggerFactory.getLogger(RequiredDocIncomeServiceImpl.class);

    private final RequiredDocIncomeRepository requiredDocIncomeRepository;

    private final RequiredDocIncomeMapper requiredDocIncomeMapper;

    public RequiredDocIncomeServiceImpl(RequiredDocIncomeRepository requiredDocIncomeRepository, RequiredDocIncomeMapper requiredDocIncomeMapper) {
        this.requiredDocIncomeRepository = requiredDocIncomeRepository;
        this.requiredDocIncomeMapper = requiredDocIncomeMapper;
    }

    @Override
    public RequiredDocIncomeDTO save(RequiredDocIncomeDTO requiredDocIncomeDTO) {
        log.debug("Request to save RequiredDocIncome : {}", requiredDocIncomeDTO);
        RequiredDocIncome requiredDocIncome = requiredDocIncomeMapper.toEntity(requiredDocIncomeDTO);
        requiredDocIncome = requiredDocIncomeRepository.save(requiredDocIncome);
        return requiredDocIncomeMapper.toDto(requiredDocIncome);
    }

    @Override
    @Transactional(readOnly = true)
    public List<RequiredDocIncomeDTO> findAll() {
        log.debug("Request to get all RequiredDocIncomes");
        return requiredDocIncomeRepository.findAll().stream()
            .map(requiredDocIncomeMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<RequiredDocIncomeDTO> findOne(Long id) {
        log.debug("Request to get RequiredDocIncome : {}", id);
        return requiredDocIncomeRepository.findById(id)
            .map(requiredDocIncomeMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete RequiredDocIncome : {}", id);
        requiredDocIncomeRepository.deleteById(id);
    }
}
