package com.attijeri.tfs.service.impl;

import com.attijeri.tfs.service.FAQService;
import com.attijeri.tfs.domain.FAQ;
import com.attijeri.tfs.repository.FAQRepository;
import com.attijeri.tfs.service.dto.FAQDTO;
import com.attijeri.tfs.service.mapper.FAQMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link FAQ}.
 */
@Service
@Transactional
public class FAQServiceImpl implements FAQService {

    private final Logger log = LoggerFactory.getLogger(FAQServiceImpl.class);

    private final FAQRepository fAQRepository;

    private final FAQMapper fAQMapper;

    public FAQServiceImpl(FAQRepository fAQRepository, FAQMapper fAQMapper) {
        this.fAQRepository = fAQRepository;
        this.fAQMapper = fAQMapper;
    }

    @Override
    public FAQDTO save(FAQDTO fAQDTO) {
        log.debug("Request to save FAQ : {}", fAQDTO);
        FAQ fAQ = fAQMapper.toEntity(fAQDTO);
        fAQ = fAQRepository.save(fAQ);
        return fAQMapper.toDto(fAQ);
    }

    @Override
    @Transactional(readOnly = true)
    public List<FAQDTO> findAll() {
        log.debug("Request to get all FAQS");
        return fAQRepository.findAll().stream()
            .map(fAQMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<FAQDTO> findOne(Long id) {
        log.debug("Request to get FAQ : {}", id);
        return fAQRepository.findById(id)
            .map(fAQMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete FAQ : {}", id);
        fAQRepository.deleteById(id);
    }
}
