package com.attijeri.tfs.service.impl;

import com.attijeri.tfs.service.GovernorateService;
import com.attijeri.tfs.domain.Governorate;
import com.attijeri.tfs.repository.GovernorateRepository;
import com.attijeri.tfs.service.dto.GovernorateDTO;
import com.attijeri.tfs.service.mapper.GovernorateMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Governorate}.
 */
@Service
@Transactional
public class GovernorateServiceImpl implements GovernorateService {

    private final Logger log = LoggerFactory.getLogger(GovernorateServiceImpl.class);

    private final GovernorateRepository governorateRepository;

    private final GovernorateMapper governorateMapper;

    public GovernorateServiceImpl(GovernorateRepository governorateRepository, GovernorateMapper governorateMapper) {
        this.governorateRepository = governorateRepository;
        this.governorateMapper = governorateMapper;
    }

    @Override
    public GovernorateDTO save(GovernorateDTO governorateDTO) {
        log.debug("Request to save Governorate : {}", governorateDTO);
        Governorate governorate = governorateMapper.toEntity(governorateDTO);
        governorate = governorateRepository.save(governorate);
        return governorateMapper.toDto(governorate);
    }

    @Override
    @Transactional(readOnly = true)
    public List<GovernorateDTO> findAll() {
        log.debug("Request to get all Governorates");
        return governorateRepository.findAll().stream()
            .map(governorateMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<GovernorateDTO> findOne(Long id) {
        log.debug("Request to get Governorate : {}", id);
        return governorateRepository.findById(id)
            .map(governorateMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Governorate : {}", id);
        governorateRepository.deleteById(id);
    }
}
