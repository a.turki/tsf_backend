package com.attijeri.tfs.service.impl;

import com.attijeri.tfs.service.RequestService;
import com.attijeri.tfs.domain.Request;
import com.attijeri.tfs.repository.RequestRepository;
import com.attijeri.tfs.service.dto.RequestDTO;
import com.attijeri.tfs.service.mapper.RequestMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service Implementation for managing {@link Request}.
 */
@Service
@Transactional
public class RequestServiceImpl implements RequestService {

    private final Logger log = LoggerFactory.getLogger(RequestServiceImpl.class);

    private final RequestRepository requestRepository;

    private final RequestMapper requestMapper;

    public RequestServiceImpl(RequestRepository requestRepository, RequestMapper requestMapper) {
        this.requestRepository = requestRepository;
        this.requestMapper = requestMapper;
    }

    @Override
    public RequestDTO save(RequestDTO requestDTO) {
        log.debug("Request to save Request : {}", requestDTO);
        Request request = requestMapper.toEntity(requestDTO);
        request = requestRepository.save(request);
        return requestMapper.toDto(request);
    }

    @Override
    @Transactional(readOnly = true)
    public List<RequestDTO> findAll() {
        log.debug("Request to get all Requests");
        return requestRepository.findAll().stream()
            .map(requestMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }



    /**
     *  Get all the requests where PersonalInfo is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true) 
    public List<RequestDTO> findAllWherePersonalInfoIsNull() {
        log.debug("Request to get all requests where PersonalInfo is null");
        return StreamSupport
            .stream(requestRepository.findAll().spliterator(), false)
            .filter(request -> request.getPersonalInfo() == null)
            .map(requestMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     *  Get all the requests where FinancialInfo is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true) 
    public List<RequestDTO> findAllWhereFinancialInfoIsNull() {
        log.debug("Request to get all requests where FinancialInfo is null");
        return StreamSupport
            .stream(requestRepository.findAll().spliterator(), false)
            .filter(request -> request.getFinancialInfo() == null)
            .map(requestMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     *  Get all the requests where RequiredDoc is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true) 
    public List<RequestDTO> findAllWhereRequiredDocIsNull() {
        log.debug("Request to get all requests where RequiredDoc is null");
        return StreamSupport
            .stream(requestRepository.findAll().spliterator(), false)
            .filter(request -> request.getRequiredDoc() == null)
            .map(requestMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<RequestDTO> findOne(Long id) {
        log.debug("Request to get Request : {}", id);
        return requestRepository.findById(id)
            .map(requestMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Request : {}", id);
        requestRepository.deleteById(id);
    }
}
