package com.attijeri.tfs.service.impl;

import com.attijeri.tfs.service.FinancialInfoService;
import com.attijeri.tfs.domain.FinancialInfo;
import com.attijeri.tfs.repository.FinancialInfoRepository;
import com.attijeri.tfs.service.dto.FinancialInfoDTO;
import com.attijeri.tfs.service.mapper.FinancialInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link FinancialInfo}.
 */
@Service
@Transactional
public class FinancialInfoServiceImpl implements FinancialInfoService {

    private final Logger log = LoggerFactory.getLogger(FinancialInfoServiceImpl.class);

    private final FinancialInfoRepository financialInfoRepository;

    private final FinancialInfoMapper financialInfoMapper;

    public FinancialInfoServiceImpl(FinancialInfoRepository financialInfoRepository, FinancialInfoMapper financialInfoMapper) {
        this.financialInfoRepository = financialInfoRepository;
        this.financialInfoMapper = financialInfoMapper;
    }

    @Override
    public FinancialInfoDTO save(FinancialInfoDTO financialInfoDTO) {
        log.debug("Request to save FinancialInfo : {}", financialInfoDTO);
        FinancialInfo financialInfo = financialInfoMapper.toEntity(financialInfoDTO);
        financialInfo = financialInfoRepository.save(financialInfo);
        return financialInfoMapper.toDto(financialInfo);
    }

    @Override
    @Transactional(readOnly = true)
    public List<FinancialInfoDTO> findAll() {
        log.debug("Request to get all FinancialInfos");
        return financialInfoRepository.findAll().stream()
            .map(financialInfoMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<FinancialInfoDTO> findOne(Long id) {
        log.debug("Request to get FinancialInfo : {}", id);
        return financialInfoRepository.findById(id)
            .map(financialInfoMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete FinancialInfo : {}", id);
        financialInfoRepository.deleteById(id);
    }
}
