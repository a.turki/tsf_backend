package com.attijeri.tfs.service.impl;

import com.attijeri.tfs.service.AddressInfoService;
import com.attijeri.tfs.domain.AddressInfo;
import com.attijeri.tfs.repository.AddressInfoRepository;
import com.attijeri.tfs.service.dto.AddressInfoDTO;
import com.attijeri.tfs.service.mapper.AddressInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service Implementation for managing {@link AddressInfo}.
 */
@Service
@Transactional
public class AddressInfoServiceImpl implements AddressInfoService {

    private final Logger log = LoggerFactory.getLogger(AddressInfoServiceImpl.class);

    private final AddressInfoRepository addressInfoRepository;

    private final AddressInfoMapper addressInfoMapper;

    public AddressInfoServiceImpl(AddressInfoRepository addressInfoRepository, AddressInfoMapper addressInfoMapper) {
        this.addressInfoRepository = addressInfoRepository;
        this.addressInfoMapper = addressInfoMapper;
    }

    @Override
    public AddressInfoDTO save(AddressInfoDTO addressInfoDTO) {
        log.debug("Request to save AddressInfo : {}", addressInfoDTO);
        AddressInfo addressInfo = addressInfoMapper.toEntity(addressInfoDTO);
        addressInfo = addressInfoRepository.save(addressInfo);
        return addressInfoMapper.toDto(addressInfo);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AddressInfoDTO> findAll() {
        log.debug("Request to get all AddressInfos");
        return addressInfoRepository.findAll().stream()
            .map(addressInfoMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }



    /**
     *  Get all the addressInfos where PersonalInfo is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true) 
    public List<AddressInfoDTO> findAllWherePersonalInfoIsNull() {
        log.debug("Request to get all addressInfos where PersonalInfo is null");
        return StreamSupport
            .stream(addressInfoRepository.findAll().spliterator(), false)
            .filter(addressInfo -> addressInfo.getPersonalInfo() == null)
            .map(addressInfoMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<AddressInfoDTO> findOne(Long id) {
        log.debug("Request to get AddressInfo : {}", id);
        return addressInfoRepository.findById(id)
            .map(addressInfoMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete AddressInfo : {}", id);
        addressInfoRepository.deleteById(id);
    }
}
