package com.attijeri.tfs.service.impl;

import com.attijeri.tfs.service.DetailOfferService;
import com.attijeri.tfs.domain.DetailOffer;
import com.attijeri.tfs.repository.DetailOfferRepository;
import com.attijeri.tfs.service.dto.DetailOfferDTO;
import com.attijeri.tfs.service.mapper.DetailOfferMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link DetailOffer}.
 */
@Service
@Transactional
public class DetailOfferServiceImpl implements DetailOfferService {

    private final Logger log = LoggerFactory.getLogger(DetailOfferServiceImpl.class);

    private final DetailOfferRepository detailOfferRepository;

    private final DetailOfferMapper detailOfferMapper;

    public DetailOfferServiceImpl(DetailOfferRepository detailOfferRepository, DetailOfferMapper detailOfferMapper) {
        this.detailOfferRepository = detailOfferRepository;
        this.detailOfferMapper = detailOfferMapper;
    }

    @Override
    public DetailOfferDTO save(DetailOfferDTO detailOfferDTO) {
        log.debug("Request to save DetailOffer : {}", detailOfferDTO);
        DetailOffer detailOffer = detailOfferMapper.toEntity(detailOfferDTO);
        detailOffer = detailOfferRepository.save(detailOffer);
        return detailOfferMapper.toDto(detailOffer);
    }

    @Override
    @Transactional(readOnly = true)
    public List<DetailOfferDTO> findAll() {
        log.debug("Request to get all DetailOffers");
        return detailOfferRepository.findAll().stream()
            .map(detailOfferMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<DetailOfferDTO> findOne(Long id) {
        log.debug("Request to get DetailOffer : {}", id);
        return detailOfferRepository.findById(id)
            .map(detailOfferMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete DetailOffer : {}", id);
        detailOfferRepository.deleteById(id);
    }
}
