package com.attijeri.tfs.service;

import com.attijeri.tfs.service.dto.RequiredDocDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.attijeri.tfs.domain.RequiredDoc}.
 */
public interface RequiredDocService {

    /**
     * Save a requiredDoc.
     *
     * @param requiredDocDTO the entity to save.
     * @return the persisted entity.
     */
    RequiredDocDTO save(RequiredDocDTO requiredDocDTO);

    /**
     * Get all the requiredDocs.
     *
     * @return the list of entities.
     */
    List<RequiredDocDTO> findAll();


    /**
     * Get the "id" requiredDoc.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RequiredDocDTO> findOne(Long id);

    /**
     * Delete the "id" requiredDoc.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
