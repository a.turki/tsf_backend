package com.attijeri.tfs.service;

import com.attijeri.tfs.service.dto.FAQDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.attijeri.tfs.domain.FAQ}.
 */
public interface FAQService {

    /**
     * Save a fAQ.
     *
     * @param fAQDTO the entity to save.
     * @return the persisted entity.
     */
    FAQDTO save(FAQDTO fAQDTO);

    /**
     * Get all the fAQS.
     *
     * @return the list of entities.
     */
    List<FAQDTO> findAll();


    /**
     * Get the "id" fAQ.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<FAQDTO> findOne(Long id);

    /**
     * Delete the "id" fAQ.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
