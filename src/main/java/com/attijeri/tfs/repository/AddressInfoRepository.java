package com.attijeri.tfs.repository;

import com.attijeri.tfs.domain.AddressInfo;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the AddressInfo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AddressInfoRepository extends JpaRepository<AddressInfo, Long> {
}
