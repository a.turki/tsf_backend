package com.attijeri.tfs.repository;

import com.attijeri.tfs.domain.RequiredDocResidency;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the RequiredDocResidency entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RequiredDocResidencyRepository extends JpaRepository<RequiredDocResidency, Long> {
}
