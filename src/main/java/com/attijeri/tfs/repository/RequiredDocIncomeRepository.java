package com.attijeri.tfs.repository;

import com.attijeri.tfs.domain.RequiredDocIncome;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the RequiredDocIncome entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RequiredDocIncomeRepository extends JpaRepository<RequiredDocIncome, Long> {
}
