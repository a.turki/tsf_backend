package com.attijeri.tfs.repository;

import com.attijeri.tfs.domain.RequiredDoc;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the RequiredDoc entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RequiredDocRepository extends JpaRepository<RequiredDoc, Long> {
}
