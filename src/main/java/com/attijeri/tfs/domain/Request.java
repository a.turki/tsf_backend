package com.attijeri.tfs.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.annotation.Nullable;
import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Request.
 */
@Entity
@Table(name = "request")
public class Request implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "visio_date")
    private LocalDate visio_date;

    @Column(name = "sending_mail_date")
    private LocalDate sendingMailDate;

    @Column(name = "state")
    private Boolean state;
    @Nullable
    @ManyToOne
    @JsonIgnoreProperties(value = "requests", allowSetters = true)
    private Offer offer;
    @Nullable
    @ManyToOne
    @JsonIgnoreProperties(value = "requests", allowSetters = true)
    private Agency agency;

    @OneToOne(mappedBy = "request",cascade = CascadeType.ALL)
    @JsonIgnore
    private PersonalInfo personalInfo;

    @OneToOne(mappedBy = "request",cascade = CascadeType.ALL)
    @JsonIgnore
    private FinancialInfo financialInfo;

    @OneToOne(mappedBy = "request",cascade = CascadeType.ALL)
    @JsonIgnore
    private RequiredDoc requiredDoc;

    @OneToMany(mappedBy = "request")
    private Set<BankAccount> bankAccounts = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getVisio_date() {
        return visio_date;
    }

    public Request visio_date(LocalDate visio_date) {
        this.visio_date = visio_date;
        return this;
    }

    public void setVisio_date(LocalDate visio_date) {
        this.visio_date = visio_date;
    }

    public LocalDate getSendingMailDate() {
        return sendingMailDate;
    }

    public Request sendingMailDate(LocalDate sendingMailDate) {
        this.sendingMailDate = sendingMailDate;
        return this;
    }

    public void setSendingMailDate(LocalDate sendingMailDate) {
        this.sendingMailDate = sendingMailDate;
    }

    public Boolean isState() {
        return state;
    }

    public Request state(Boolean state) {
        this.state = state;
        return this;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public Offer getOffer() {
        return offer;
    }

    public Request offer(Offer offer) {
        this.offer = offer;
        return this;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    public Agency getAgency() {
        return agency;
    }

    public Request agency(Agency agency) {
        this.agency = agency;
        return this;
    }

    public void setAgency(Agency agency) {
        this.agency = agency;
    }

    public PersonalInfo getPersonalInfo() {
        return personalInfo;
    }

    public Request personalInfo(PersonalInfo personalInfo) {
        this.personalInfo = personalInfo;
        return this;
    }

    public void setPersonalInfo(PersonalInfo personalInfo) {
        this.personalInfo = personalInfo;
    }

    public FinancialInfo getFinancialInfo() {
        return financialInfo;
    }

    public Request financialInfo(FinancialInfo financialInfo) {
        this.financialInfo = financialInfo;
        return this;
    }

    public void setFinancialInfo(FinancialInfo financialInfo) {
        this.financialInfo = financialInfo;
    }

    public RequiredDoc getRequiredDoc() {
        return requiredDoc;
    }

    public Request requiredDoc(RequiredDoc requiredDoc) {
        this.requiredDoc = requiredDoc;
        return this;
    }

    public void setRequiredDoc(RequiredDoc requiredDoc) {
        this.requiredDoc = requiredDoc;
    }

    public Set<BankAccount> getBankAccounts() {
        return bankAccounts;
    }

    public Request bankAccounts(Set<BankAccount> bankAccounts) {
        this.bankAccounts = bankAccounts;
        return this;
    }

    public Request addBankAccount(BankAccount bankAccount) {
        this.bankAccounts.add(bankAccount);
        bankAccount.setRequest(this);
        return this;
    }

    public Request removeBankAccount(BankAccount bankAccount) {
        this.bankAccounts.remove(bankAccount);
        bankAccount.setRequest(null);
        return this;
    }

    public void setBankAccounts(Set<BankAccount> bankAccounts) {
        this.bankAccounts = bankAccounts;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Request)) {
            return false;
        }
        return id != null && id.equals(((Request) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Request{" +
            "id=" + getId() +
            ", visio_date='" + getVisio_date() + "'" +
            ", sendingMailDate='" + getSendingMailDate() + "'" +
            ", state='" + isState() + "'" +
            "}";
    }
}
