package com.attijeri.tfs.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A RequiredDocResidency.
 */
@Entity
@Table(name = "doc_residency")
public class RequiredDocResidency implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "jhi_type")
    private String type;

    @Column(name = "num")
    private String num;

    @Column(name = "delivery_date")
    private LocalDate deliveryDate;

    @Column(name = "expiration_date")
    private LocalDate expirationDate;

    @Column(name = "illimited_expiration_date")
    private Boolean illimitedExpirationDate;

    @Column(name = "path")
    private String path;

    @ManyToOne
    @JsonIgnoreProperties(value = "requiredDocResidencies", allowSetters = true)
    private RequiredDoc requiredDoc;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public RequiredDocResidency type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNum() {
        return num;
    }

    public RequiredDocResidency num(String num) {
        this.num = num;
        return this;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public LocalDate getDeliveryDate() {
        return deliveryDate;
    }

    public RequiredDocResidency deliveryDate(LocalDate deliveryDate) {
        this.deliveryDate = deliveryDate;
        return this;
    }

    public void setDeliveryDate(LocalDate deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public RequiredDocResidency expirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
        return this;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Boolean isIllimitedExpirationDate() {
        return illimitedExpirationDate;
    }

    public RequiredDocResidency illimitedExpirationDate(Boolean illimitedExpirationDate) {
        this.illimitedExpirationDate = illimitedExpirationDate;
        return this;
    }

    public void setIllimitedExpirationDate(Boolean illimitedExpirationDate) {
        this.illimitedExpirationDate = illimitedExpirationDate;
    }

    public String getPath() {
        return path;
    }

    public RequiredDocResidency path(String path) {
        this.path = path;
        return this;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public RequiredDoc getRequiredDoc() {
        return requiredDoc;
    }

    public RequiredDocResidency requiredDoc(RequiredDoc requiredDoc) {
        this.requiredDoc = requiredDoc;
        return this;
    }

    public void setRequiredDoc(RequiredDoc requiredDoc) {
        this.requiredDoc = requiredDoc;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RequiredDocResidency)) {
            return false;
        }
        return id != null && id.equals(((RequiredDocResidency) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RequiredDocResidency{" +
            "id=" + getId() +
            ", type='" + getType() + "'" +
            ", num='" + getNum() + "'" +
            ", deliveryDate='" + getDeliveryDate() + "'" +
            ", expirationDate='" + getExpirationDate() + "'" +
            ", illimitedExpirationDate='" + isIllimitedExpirationDate() + "'" +
            ", path='" + getPath() + "'" +
            "}";
    }
}
