package com.attijeri.tfs.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A FinancialInfo.
 */
@Entity
@Table(name = "financial_info")
public class FinancialInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "monthly_net_income")
    private Double monthlyNetIncome;

    @ManyToOne
    @JsonIgnoreProperties(value = "financialInfos", allowSetters = true)
    private Category category;

    @ManyToOne
    @JsonIgnoreProperties(value = "financialInfos", allowSetters = true)
    private Activity activity;

    @OneToOne
    @JoinColumn(unique = true)
    private Request request;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getMonthlyNetIncome() {
        return monthlyNetIncome;
    }

    public FinancialInfo monthlyNetIncome(Double monthlyNetIncome) {
        this.monthlyNetIncome = monthlyNetIncome;
        return this;
    }

    public void setMonthlyNetIncome(Double monthlyNetIncome) {
        this.monthlyNetIncome = monthlyNetIncome;
    }

    public Category getCategory() {
        return category;
    }

    public FinancialInfo category(Category category) {
        this.category = category;
        return this;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Activity getActivity() {
        return activity;
    }

    public FinancialInfo activity(Activity activity) {
        this.activity = activity;
        return this;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public Request getRequest() {
        return request;
    }

    public FinancialInfo request(Request request) {
        this.request = request;
        return this;
    }

    public void setRequest(Request request) {
        this.request = request;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FinancialInfo)) {
            return false;
        }
        return id != null && id.equals(((FinancialInfo) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FinancialInfo{" +
            "id=" + getId() +
            ", monthlyNetIncome=" + getMonthlyNetIncome() +
            "}";
    }
}
