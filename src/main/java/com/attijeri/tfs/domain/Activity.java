package com.attijeri.tfs.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Activity.
 */
@Entity
@Table(name = "activity")
public class Activity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "activity_name")
    private String activityName;

    @OneToMany(mappedBy = "activity")
    private Set<FinancialInfo> financialInfos = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getActivityName() {
        return activityName;
    }

    public Activity activityName(String activityName) {
        this.activityName = activityName;
        return this;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public Set<FinancialInfo> getFinancialInfos() {
        return financialInfos;
    }

    public Activity financialInfos(Set<FinancialInfo> financialInfos) {
        this.financialInfos = financialInfos;
        return this;
    }

    public Activity addFinancialInfo(FinancialInfo financialInfo) {
        this.financialInfos.add(financialInfo);
        financialInfo.setActivity(this);
        return this;
    }

    public Activity removeFinancialInfo(FinancialInfo financialInfo) {
        this.financialInfos.remove(financialInfo);
        financialInfo.setActivity(null);
        return this;
    }

    public void setFinancialInfos(Set<FinancialInfo> financialInfos) {
        this.financialInfos = financialInfos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Activity)) {
            return false;
        }
        return id != null && id.equals(((Activity) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Activity{" +
            "id=" + getId() +
            ", activityName='" + getActivityName() + "'" +
            "}";
    }
}
