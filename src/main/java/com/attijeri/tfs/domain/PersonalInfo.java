package com.attijeri.tfs.domain;


import javax.annotation.Nullable;
import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

import com.attijeri.tfs.domain.enumeration.Civility;

/**
 * A PersonalInfo.
 */
@Entity
@Table(name = "personal_info")
public class PersonalInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;
    @Nullable
    @Enumerated(EnumType.STRING)
    @Column(name = "civility")
    private Civility civility;
    @Nullable
    @Column(name = "first_name")
    private String firstName;
    @Nullable
    @Column(name = "last_name")
    private String lastName;
    @Nullable
    @Column(name = "email")
    private String email;
    @Nullable
    @Column(name = "phone")
    private Long phone;
    @Nullable
    @Column(name = "native_country")
    private String nativeCountry;
    @Nullable
    @Column(name = "birthday")
    private LocalDate birthday;
    @Nullable
    @Column(name = "client_abt")
    private Boolean clientABT;
    @Nullable
    @Column(name = "rib")
    private String rib;
    @Nullable
    @Column(name = "nationality")
    private String nationality;
    @Nullable
    @Column(name = "second_nationality")
    private String secondNationality;
    @Nullable
    @Column(name = "marital_status")
    private String maritalStatus;
    @Nullable
    @Column(name = "nbr_kids")
    private Integer nbrKids;
    @Nullable
    @Column(name = "american_index")
    private String americanIndex;
    @Nullable
    @OneToOne
    @JoinColumn(unique = true)
    private AddressInfo addressInfo;
    @Nullable
    @OneToOne
    @JoinColumn(unique = true)
    private Request request;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Civility getCivility() {
        return civility;
    }

    public PersonalInfo civility(Civility civility) {
        this.civility = civility;
        return this;
    }

    public void setCivility(Civility civility) {
        this.civility = civility;
    }

    public String getFirstName() {
        return firstName;
    }

    public PersonalInfo firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public PersonalInfo lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public PersonalInfo email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getPhone() {
        return phone;
    }

    public PersonalInfo phone(Long phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public String getNativeCountry() {
        return nativeCountry;
    }

    public PersonalInfo nativeCountry(String nativeCountry) {
        this.nativeCountry = nativeCountry;
        return this;
    }

    public void setNativeCountry(String nativeCountry) {
        this.nativeCountry = nativeCountry;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public PersonalInfo birthday(LocalDate birthday) {
        this.birthday = birthday;
        return this;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public Boolean isClientABT() {
        return clientABT;
    }

    public PersonalInfo clientABT(Boolean clientABT) {
        this.clientABT = clientABT;
        return this;
    }

    public void setClientABT(Boolean clientABT) {
        this.clientABT = clientABT;
    }

    public String getRib() {
        return rib;
    }

    public PersonalInfo rib(String rib) {
        this.rib = rib;
        return this;
    }

    public void setRib(String rib) {
        this.rib = rib;
    }

    public String getNationality() {
        return nationality;
    }

    public PersonalInfo nationality(String nationality) {
        this.nationality = nationality;
        return this;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getSecondNationality() {
        return secondNationality;
    }

    public PersonalInfo secondNationality(String secondNationality) {
        this.secondNationality = secondNationality;
        return this;
    }

    public void setSecondNationality(String secondNationality) {
        this.secondNationality = secondNationality;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public PersonalInfo maritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
        return this;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public Integer getNbrKids() {
        return nbrKids;
    }

    public PersonalInfo nbrKids(Integer nbrKids) {
        this.nbrKids = nbrKids;
        return this;
    }

    public void setNbrKids(Integer nbrKids) {
        this.nbrKids = nbrKids;
    }

    public String getAmericanIndex() {
        return americanIndex;
    }

    public PersonalInfo americanIndex(String americanIndex) {
        this.americanIndex = americanIndex;
        return this;
    }

    public void setAmericanIndex(String americanIndex) {
        this.americanIndex = americanIndex;
    }

    public AddressInfo getAddressInfo() {
        return addressInfo;
    }

    public PersonalInfo addressInfo(AddressInfo addressInfo) {
        this.addressInfo = addressInfo;
        return this;
    }

    public void setAddressInfo(AddressInfo addressInfo) {
        this.addressInfo = addressInfo;
    }

    public Request getRequest() {
        return request;
    }

    public PersonalInfo request(Request request) {
        this.request = request;
        return this;
    }

    public void setRequest(Request request) {
        this.request = request;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PersonalInfo)) {
            return false;
        }
        return id != null && id.equals(((PersonalInfo) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PersonalInfo{" +
            "id=" + getId() +
            ", civility='" + getCivility() + "'" +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", email='" + getEmail() + "'" +
            ", phone=" + getPhone() +
            ", nativeCountry='" + getNativeCountry() + "'" +
            ", birthday='" + getBirthday() + "'" +
            ", clientABT='" + isClientABT() + "'" +
            ", rib='" + getRib() + "'" +
            ", nationality='" + getNationality() + "'" +
            ", secondNationality='" + getSecondNationality() + "'" +
            ", maritalStatus='" + getMaritalStatus() + "'" +
            ", nbrKids=" + getNbrKids() +
            ", americanIndex='" + getAmericanIndex() + "'" +
            "}";
    }
}
