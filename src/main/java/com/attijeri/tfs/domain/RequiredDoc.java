package com.attijeri.tfs.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A RequiredDoc.
 */
@Entity
@Table(name = "required_doc")
public class RequiredDoc implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "jhi_label")
    private String label;

    @Column(name = "jhi_type")
    private String type;

    @Column(name = "num_cin")
    private String numCIN;

    @Column(name = "delivery_date_cin")
    private LocalDate deliveryDateCIN;

    @Column(name = "recto_cin")
    private String rectoCIN;

    @Column(name = "verso_cin")
    private String versoCIN;

    @Column(name = "facta")
    private String facta;

    @OneToMany(mappedBy = "requiredDoc")
    private Set<RequiredDocIncome> requiredDocIncomes = new HashSet<>();

    @OneToMany(mappedBy = "requiredDoc")
    private Set<RequiredDocResidency> requiredDocResidencies = new HashSet<>();

    @OneToOne
    @JoinColumn(unique = true)
    private Request request;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public RequiredDoc label(String label) {
        this.label = label;
        return this;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getType() {
        return type;
    }

    public RequiredDoc type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNumCIN() {
        return numCIN;
    }

    public RequiredDoc numCIN(String numCIN) {
        this.numCIN = numCIN;
        return this;
    }

    public void setNumCIN(String numCIN) {
        this.numCIN = numCIN;
    }

    public LocalDate getDeliveryDateCIN() {
        return deliveryDateCIN;
    }

    public RequiredDoc deliveryDateCIN(LocalDate deliveryDateCIN) {
        this.deliveryDateCIN = deliveryDateCIN;
        return this;
    }

    public void setDeliveryDateCIN(LocalDate deliveryDateCIN) {
        this.deliveryDateCIN = deliveryDateCIN;
    }

    public String getRectoCIN() {
        return rectoCIN;
    }

    public RequiredDoc rectoCIN(String rectoCIN) {
        this.rectoCIN = rectoCIN;
        return this;
    }

    public void setRectoCIN(String rectoCIN) {
        this.rectoCIN = rectoCIN;
    }

    public String getVersoCIN() {
        return versoCIN;
    }

    public RequiredDoc versoCIN(String versoCIN) {
        this.versoCIN = versoCIN;
        return this;
    }

    public void setVersoCIN(String versoCIN) {
        this.versoCIN = versoCIN;
    }

    public String getFacta() {
        return facta;
    }

    public RequiredDoc facta(String facta) {
        this.facta = facta;
        return this;
    }

    public void setFacta(String facta) {
        this.facta = facta;
    }

    public Set<RequiredDocIncome> getRequiredDocIncomes() {
        return requiredDocIncomes;
    }

    public RequiredDoc requiredDocIncomes(Set<RequiredDocIncome> requiredDocIncomes) {
        this.requiredDocIncomes = requiredDocIncomes;
        return this;
    }

    public RequiredDoc addRequiredDocIncome(RequiredDocIncome requiredDocIncome) {
        this.requiredDocIncomes.add(requiredDocIncome);
        requiredDocIncome.setRequiredDoc(this);
        return this;
    }

    public RequiredDoc removeRequiredDocIncome(RequiredDocIncome requiredDocIncome) {
        this.requiredDocIncomes.remove(requiredDocIncome);
        requiredDocIncome.setRequiredDoc(null);
        return this;
    }

    public void setRequiredDocIncomes(Set<RequiredDocIncome> requiredDocIncomes) {
        this.requiredDocIncomes = requiredDocIncomes;
    }

    public Set<RequiredDocResidency> getRequiredDocResidencies() {
        return requiredDocResidencies;
    }

    public RequiredDoc requiredDocResidencies(Set<RequiredDocResidency> requiredDocResidencies) {
        this.requiredDocResidencies = requiredDocResidencies;
        return this;
    }

    public RequiredDoc addRequiredDocResidency(RequiredDocResidency requiredDocResidency) {
        this.requiredDocResidencies.add(requiredDocResidency);
        requiredDocResidency.setRequiredDoc(this);
        return this;
    }

    public RequiredDoc removeRequiredDocResidency(RequiredDocResidency requiredDocResidency) {
        this.requiredDocResidencies.remove(requiredDocResidency);
        requiredDocResidency.setRequiredDoc(null);
        return this;
    }

    public void setRequiredDocResidencies(Set<RequiredDocResidency> requiredDocResidencies) {
        this.requiredDocResidencies = requiredDocResidencies;
    }

    public Request getRequest() {
        return request;
    }

    public RequiredDoc request(Request request) {
        this.request = request;
        return this;
    }

    public void setRequest(Request request) {
        this.request = request;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RequiredDoc)) {
            return false;
        }
        return id != null && id.equals(((RequiredDoc) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RequiredDoc{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", type='" + getType() + "'" +
            ", numCIN='" + getNumCIN() + "'" +
            ", deliveryDateCIN='" + getDeliveryDateCIN() + "'" +
            ", rectoCIN='" + getRectoCIN() + "'" +
            ", versoCIN='" + getVersoCIN() + "'" +
            ", facta='" + getFacta() + "'" +
            "}";
    }
}
