package com.attijeri.tfs.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.annotation.Nullable;
import javax.persistence.*;

import java.io.Serializable;

/**
 * A AddressInfo.
 */
@Entity
@Table(name = "address_info")
public class AddressInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "country_of_residence")
    private String countryOfResidence;

    @Column(name = "address")
    private String address;

    @Column(name = "zip")
    private Integer zip;

    @Column(name = "city")
    private String city;

    @Nullable
    @OneToOne(mappedBy = "addressInfo")
    @JsonIgnore
    private PersonalInfo personalInfo;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountryOfResidence() {
        return countryOfResidence;
    }

    public AddressInfo countryOfResidence(String countryOfResidence) {
        this.countryOfResidence = countryOfResidence;
        return this;
    }

    public void setCountryOfResidence(String countryOfResidence) {
        this.countryOfResidence = countryOfResidence;
    }

    public String getAddress() {
        return address;
    }

    public AddressInfo address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getZip() {
        return zip;
    }

    public AddressInfo zip(Integer zip) {
        this.zip = zip;
        return this;
    }

    public void setZip(Integer zip) {
        this.zip = zip;
    }

    public String getCity() {
        return city;
    }

    public AddressInfo city(String city) {
        this.city = city;
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public PersonalInfo getPersonalInfo() {
        return personalInfo;
    }

    public AddressInfo personalInfo(PersonalInfo personalInfo) {
        this.personalInfo = personalInfo;
        return this;
    }

    public void setPersonalInfo(PersonalInfo personalInfo) {
        this.personalInfo = personalInfo;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AddressInfo)) {
            return false;
        }
        return id != null && id.equals(((AddressInfo) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AddressInfo{" +
            "id=" + getId() +
            ", countryOfResidence='" + getCountryOfResidence() + "'" +
            ", address='" + getAddress() + "'" +
            ", zip=" + getZip() +
            ", city='" + getCity() + "'" +
            "}";
    }
}
