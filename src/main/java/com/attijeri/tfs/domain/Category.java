package com.attijeri.tfs.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Category.
 */
@Entity
@Table(name = "category")
public class Category implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "category_name")
    private String categoryName;

    @OneToMany(mappedBy = "category")
    private Set<FinancialInfo> financialInfos = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public Category categoryName(String categoryName) {
        this.categoryName = categoryName;
        return this;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Set<FinancialInfo> getFinancialInfos() {
        return financialInfos;
    }

    public Category financialInfos(Set<FinancialInfo> financialInfos) {
        this.financialInfos = financialInfos;
        return this;
    }

    public Category addFinancialInfo(FinancialInfo financialInfo) {
        this.financialInfos.add(financialInfo);
        financialInfo.setCategory(this);
        return this;
    }

    public Category removeFinancialInfo(FinancialInfo financialInfo) {
        this.financialInfos.remove(financialInfo);
        financialInfo.setCategory(null);
        return this;
    }

    public void setFinancialInfos(Set<FinancialInfo> financialInfos) {
        this.financialInfos = financialInfos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Category)) {
            return false;
        }
        return id != null && id.equals(((Category) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Category{" +
            "id=" + getId() +
            ", categoryName='" + getCategoryName() + "'" +
            "}";
    }
}
