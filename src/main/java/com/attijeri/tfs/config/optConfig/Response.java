package com.attijeri.tfs.config.optConfig;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class Response {
    @XmlElement
    private DocumentHeader documentHeader;

    public Response(DocumentHeader documentHeader) {
        this.documentHeader = documentHeader;
    }

    public Response() {
    }

    public DocumentHeader getDocumentHeader() {
        return documentHeader;
    }

    public void setDocumentHeader(DocumentHeader documentHeader) {
        this.documentHeader = documentHeader;
    }
}
