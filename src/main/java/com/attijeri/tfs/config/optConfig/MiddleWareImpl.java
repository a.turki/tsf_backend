package com.attijeri.tfs.config.optConfig;

import com.attijeri.tfs.domain.User;
import com.attijeri.tfs.service.dto.PersonalInfoDTO;
import com.attijeri.tfs.service.dto.RequestDTO;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.github.jhipster.config.JHipsterProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import java.util.Base64;
import java.util.Locale;

@Service
public class MiddleWareImpl implements MiddleWareService {

    private  static final Logger log = LoggerFactory.getLogger(MiddleWareImpl.class);

    @Value("${middleware.protocol}")
    String middlewareProtocol;

    @Value("${middleware.host}")
    String middlewareHost;

    @Value("${middleware.port}")
    String middlewarePort;

    @Value("${middleware.username}")
    String middlewareUserName;

    @Value("${middleware.password}")
    String middlewarePassword;

    @Value("${middleware.uri.systeme}")
    String middlewareSystemeUri;

    @Value("${middleware.authorization}")
    String authorization;

    @Value("${middleware.uri.delta}")
    String middlewareDeltaUri;

    @Value("${java.naming.factory.initial}")
    String ldapCtxFactory;

    @Value("${java.naming.security.authentication}")
    String ldapAuthentication;

    @Value("${java.naming.referral}")
    String ldapReferral;

    @Value("${java.naming.provider.url}")
    String ldapUrl;

    @Value("${java.naming.security.principal}")
    String ldapPrincipal;

    @Value("${middleware.uri.idc}")
    String middlewareIdcUri;

    private final SpringTemplateEngine templateEngine;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
    private final JHipsterProperties jHipsterProperties;

    public MiddleWareImpl(SpringTemplateEngine templateEngine, JHipsterProperties jHipsterProperties) {
        this.templateEngine = templateEngine;

        this.jHipsterProperties = jHipsterProperties;
    }

    class Email {
        String to;
        String cc;
        String bcc;
        String subject;
        String  content;
        public Email(){

        }
        public Email (String to, String cc, String cci, String subject, String content) {
            this.to = to;
            this.cc = cc;
            this.bcc = cci;
            this.subject = subject;
            this.content = content;
        }

        @Override
        public String toString() {
            return "Email{" +
                "to='" + to + '\'' +
                ", cc='" + cc + '\'' +
                ", bcc='" + bcc + '\'' +
                ", subject='" + subject + '\'' +
                ", content='" + content + '\'' +
                '}';
        }
    }

    private String uriEmail(){
        return middlewareProtocol + middlewareHost + ":" + middlewarePort + "/" + middlewareSystemeUri + "/email/sendSimpleEmail";
    }

    private String createContentFromTemplate(String templateName, Context context) {
        return templateEngine.process(templateName, context);
    }

    @Override
    public ResponseEntity<Response> sendEmail(String to, String cc, String cci, String subject, String content) {
        Email email = new Email(to, cc, cci, subject, content);
        log.debug("Request to send Email {}", email);
        return restTemplate().exchange(uriEmail(), HttpMethod.POST, createRequest(email), Response.class);
    }

    @Override
    public ResponseEntity<Response> sendConfirmationEmail(PersonalInfoDTO personalInfoDTO, String subject) {
        Context context = new Context();
        //  Context context = new Context(Locale.forLanguageTag(user.getLangKey()));
        context.setVariable("user", personalInfoDTO.getLastName()+""+personalInfoDTO.getFirstName());
        context.setVariable("personalDTO", personalInfoDTO);
        context.setVariable("title", subject);
        context.setVariable("email","http://localhost:4200/step4/"+ personalInfoDTO.getRequestId());
        context.setVariable("baseUrl", jHipsterProperties.getMail().getBaseUrl());
        return sendEmail(personalInfoDTO.getEmail(), null, null, subject, createContentFromTemplate("mail/confirmationMail", context));
    }

    private HttpEntity<Object> createRequest(Email email) {
        ObjectMapper mapper = new ObjectMapper();
        String requestJson = null;
        try {
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            requestJson = mapper.writeValueAsString(email);
        } catch (Exception e) {
            log.error("JsonGenerationException occurred ", e);
        }
        HttpEntity<Object> request = new HttpEntity<Object>(requestJson, createHeaders());
        return request;
    }

    private HttpHeaders createHeaders(){
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Basic " + Base64.getEncoder().encodeToString((middlewareUserName + ":" + middlewarePassword).getBytes() ));
        httpHeaders.add("Content-Type", MediaType.APPLICATION_JSON_VALUE );
        return httpHeaders;
    }
}
