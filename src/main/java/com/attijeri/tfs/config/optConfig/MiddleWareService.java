package com.attijeri.tfs.config.optConfig;

import com.attijeri.tfs.domain.User;
import com.attijeri.tfs.service.dto.PersonalInfoDTO;
import com.attijeri.tfs.service.dto.RequestDTO;
import org.springframework.http.ResponseEntity;

public interface MiddleWareService {

    ResponseEntity<Response> sendEmail(String to, String cc, String cci, String subject, String content);

    ResponseEntity<Response> sendConfirmationEmail(PersonalInfoDTO personalInfoDTO,String subject);
}
