package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.TfsRecetteApp;
import com.attijeri.tfs.domain.RequiredDocResidency;
import com.attijeri.tfs.repository.RequiredDocResidencyRepository;
import com.attijeri.tfs.service.RequiredDocResidencyService;
import com.attijeri.tfs.service.dto.RequiredDocResidencyDTO;
import com.attijeri.tfs.service.mapper.RequiredDocResidencyMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RequiredDocResidencyResource} REST controller.
 */
@SpringBootTest(classes = TfsRecetteApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class RequiredDocResidencyResourceIT {

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_NUM = "AAAAAAAAAA";
    private static final String UPDATED_NUM = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DELIVERY_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DELIVERY_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_EXPIRATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_EXPIRATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_ILLIMITED_EXPIRATION_DATE = false;
    private static final Boolean UPDATED_ILLIMITED_EXPIRATION_DATE = true;

    private static final String DEFAULT_PATH = "AAAAAAAAAA";
    private static final String UPDATED_PATH = "BBBBBBBBBB";

    @Autowired
    private RequiredDocResidencyRepository requiredDocResidencyRepository;

    @Autowired
    private RequiredDocResidencyMapper requiredDocResidencyMapper;

    @Autowired
    private RequiredDocResidencyService requiredDocResidencyService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restRequiredDocResidencyMockMvc;

    private RequiredDocResidency requiredDocResidency;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RequiredDocResidency createEntity(EntityManager em) {
        RequiredDocResidency requiredDocResidency = new RequiredDocResidency()
            .type(DEFAULT_TYPE)
            .num(DEFAULT_NUM)
            .deliveryDate(DEFAULT_DELIVERY_DATE)
            .expirationDate(DEFAULT_EXPIRATION_DATE)
            .illimitedExpirationDate(DEFAULT_ILLIMITED_EXPIRATION_DATE)
            .path(DEFAULT_PATH);
        return requiredDocResidency;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RequiredDocResidency createUpdatedEntity(EntityManager em) {
        RequiredDocResidency requiredDocResidency = new RequiredDocResidency()
            .type(UPDATED_TYPE)
            .num(UPDATED_NUM)
            .deliveryDate(UPDATED_DELIVERY_DATE)
            .expirationDate(UPDATED_EXPIRATION_DATE)
            .illimitedExpirationDate(UPDATED_ILLIMITED_EXPIRATION_DATE)
            .path(UPDATED_PATH);
        return requiredDocResidency;
    }

    @BeforeEach
    public void initTest() {
        requiredDocResidency = createEntity(em);
    }

    @Test
    @Transactional
    public void getAllRequiredDocResidencies() throws Exception {
        // Initialize the database
        requiredDocResidencyRepository.saveAndFlush(requiredDocResidency);

        // Get all the requiredDocResidencyList
        restRequiredDocResidencyMockMvc.perform(get("/api/required-doc-residencies?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(requiredDocResidency.getId().intValue())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)))
            .andExpect(jsonPath("$.[*].num").value(hasItem(DEFAULT_NUM)))
            .andExpect(jsonPath("$.[*].deliveryDate").value(hasItem(DEFAULT_DELIVERY_DATE.toString())))
            .andExpect(jsonPath("$.[*].expirationDate").value(hasItem(DEFAULT_EXPIRATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].illimitedExpirationDate").value(hasItem(DEFAULT_ILLIMITED_EXPIRATION_DATE.booleanValue())))
            .andExpect(jsonPath("$.[*].path").value(hasItem(DEFAULT_PATH)));
    }
    
    @Test
    @Transactional
    public void getRequiredDocResidency() throws Exception {
        // Initialize the database
        requiredDocResidencyRepository.saveAndFlush(requiredDocResidency);

        // Get the requiredDocResidency
        restRequiredDocResidencyMockMvc.perform(get("/api/required-doc-residencies/{id}", requiredDocResidency.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(requiredDocResidency.getId().intValue()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE))
            .andExpect(jsonPath("$.num").value(DEFAULT_NUM))
            .andExpect(jsonPath("$.deliveryDate").value(DEFAULT_DELIVERY_DATE.toString()))
            .andExpect(jsonPath("$.expirationDate").value(DEFAULT_EXPIRATION_DATE.toString()))
            .andExpect(jsonPath("$.illimitedExpirationDate").value(DEFAULT_ILLIMITED_EXPIRATION_DATE.booleanValue()))
            .andExpect(jsonPath("$.path").value(DEFAULT_PATH));
    }
    @Test
    @Transactional
    public void getNonExistingRequiredDocResidency() throws Exception {
        // Get the requiredDocResidency
        restRequiredDocResidencyMockMvc.perform(get("/api/required-doc-residencies/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }
}
