package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.TfsRecetteApp;
import com.attijeri.tfs.domain.RequiredDocIncome;
import com.attijeri.tfs.repository.RequiredDocIncomeRepository;
import com.attijeri.tfs.service.RequiredDocIncomeService;
import com.attijeri.tfs.service.dto.RequiredDocIncomeDTO;
import com.attijeri.tfs.service.mapper.RequiredDocIncomeMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RequiredDocIncomeResource} REST controller.
 */
@SpringBootTest(classes = TfsRecetteApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class RequiredDocIncomeResourceIT {

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_PATH = "AAAAAAAAAA";
    private static final String UPDATED_PATH = "BBBBBBBBBB";

    @Autowired
    private RequiredDocIncomeRepository requiredDocIncomeRepository;

    @Autowired
    private RequiredDocIncomeMapper requiredDocIncomeMapper;

    @Autowired
    private RequiredDocIncomeService requiredDocIncomeService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restRequiredDocIncomeMockMvc;

    private RequiredDocIncome requiredDocIncome;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RequiredDocIncome createEntity(EntityManager em) {
        RequiredDocIncome requiredDocIncome = new RequiredDocIncome()
            .type(DEFAULT_TYPE)
            .path(DEFAULT_PATH);
        return requiredDocIncome;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RequiredDocIncome createUpdatedEntity(EntityManager em) {
        RequiredDocIncome requiredDocIncome = new RequiredDocIncome()
            .type(UPDATED_TYPE)
            .path(UPDATED_PATH);
        return requiredDocIncome;
    }

    @BeforeEach
    public void initTest() {
        requiredDocIncome = createEntity(em);
    }

    @Test
    @Transactional
    public void getAllRequiredDocIncomes() throws Exception {
        // Initialize the database
        requiredDocIncomeRepository.saveAndFlush(requiredDocIncome);

        // Get all the requiredDocIncomeList
        restRequiredDocIncomeMockMvc.perform(get("/api/required-doc-incomes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(requiredDocIncome.getId().intValue())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)))
            .andExpect(jsonPath("$.[*].path").value(hasItem(DEFAULT_PATH)));
    }
    
    @Test
    @Transactional
    public void getRequiredDocIncome() throws Exception {
        // Initialize the database
        requiredDocIncomeRepository.saveAndFlush(requiredDocIncome);

        // Get the requiredDocIncome
        restRequiredDocIncomeMockMvc.perform(get("/api/required-doc-incomes/{id}", requiredDocIncome.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(requiredDocIncome.getId().intValue()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE))
            .andExpect(jsonPath("$.path").value(DEFAULT_PATH));
    }
    @Test
    @Transactional
    public void getNonExistingRequiredDocIncome() throws Exception {
        // Get the requiredDocIncome
        restRequiredDocIncomeMockMvc.perform(get("/api/required-doc-incomes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }
}
