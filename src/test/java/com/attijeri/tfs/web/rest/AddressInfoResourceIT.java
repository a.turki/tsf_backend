package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.TfsRecetteApp;
import com.attijeri.tfs.domain.AddressInfo;
import com.attijeri.tfs.repository.AddressInfoRepository;
import com.attijeri.tfs.service.AddressInfoService;
import com.attijeri.tfs.service.dto.AddressInfoDTO;
import com.attijeri.tfs.service.mapper.AddressInfoMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AddressInfoResource} REST controller.
 */
@SpringBootTest(classes = TfsRecetteApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class AddressInfoResourceIT {

    private static final String DEFAULT_COUNTRY_OF_RESIDENCE = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY_OF_RESIDENCE = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final Integer DEFAULT_ZIP = 1;
    private static final Integer UPDATED_ZIP = 2;

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    @Autowired
    private AddressInfoRepository addressInfoRepository;

    @Autowired
    private AddressInfoMapper addressInfoMapper;

    @Autowired
    private AddressInfoService addressInfoService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAddressInfoMockMvc;

    private AddressInfo addressInfo;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AddressInfo createEntity(EntityManager em) {
        AddressInfo addressInfo = new AddressInfo()
            .countryOfResidence(DEFAULT_COUNTRY_OF_RESIDENCE)
            .address(DEFAULT_ADDRESS)
            .zip(DEFAULT_ZIP)
            .city(DEFAULT_CITY);
        return addressInfo;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AddressInfo createUpdatedEntity(EntityManager em) {
        AddressInfo addressInfo = new AddressInfo()
            .countryOfResidence(UPDATED_COUNTRY_OF_RESIDENCE)
            .address(UPDATED_ADDRESS)
            .zip(UPDATED_ZIP)
            .city(UPDATED_CITY);
        return addressInfo;
    }

    @BeforeEach
    public void initTest() {
        addressInfo = createEntity(em);
    }

    @Test
    @Transactional
    public void getAllAddressInfos() throws Exception {
        // Initialize the database
        addressInfoRepository.saveAndFlush(addressInfo);

        // Get all the addressInfoList
        restAddressInfoMockMvc.perform(get("/api/address-infos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(addressInfo.getId().intValue())))
            .andExpect(jsonPath("$.[*].countryOfResidence").value(hasItem(DEFAULT_COUNTRY_OF_RESIDENCE)))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
            .andExpect(jsonPath("$.[*].zip").value(hasItem(DEFAULT_ZIP)))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY)));
    }
    
    @Test
    @Transactional
    public void getAddressInfo() throws Exception {
        // Initialize the database
        addressInfoRepository.saveAndFlush(addressInfo);

        // Get the addressInfo
        restAddressInfoMockMvc.perform(get("/api/address-infos/{id}", addressInfo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(addressInfo.getId().intValue()))
            .andExpect(jsonPath("$.countryOfResidence").value(DEFAULT_COUNTRY_OF_RESIDENCE))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS))
            .andExpect(jsonPath("$.zip").value(DEFAULT_ZIP))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY));
    }
    @Test
    @Transactional
    public void getNonExistingAddressInfo() throws Exception {
        // Get the addressInfo
        restAddressInfoMockMvc.perform(get("/api/address-infos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }
}
