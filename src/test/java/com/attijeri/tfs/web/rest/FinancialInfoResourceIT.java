package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.TfsRecetteApp;
import com.attijeri.tfs.domain.FinancialInfo;
import com.attijeri.tfs.repository.FinancialInfoRepository;
import com.attijeri.tfs.service.FinancialInfoService;
import com.attijeri.tfs.service.dto.FinancialInfoDTO;
import com.attijeri.tfs.service.mapper.FinancialInfoMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FinancialInfoResource} REST controller.
 */
@SpringBootTest(classes = TfsRecetteApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class FinancialInfoResourceIT {

    private static final Double DEFAULT_MONTHLY_NET_INCOME = 1D;
    private static final Double UPDATED_MONTHLY_NET_INCOME = 2D;

    @Autowired
    private FinancialInfoRepository financialInfoRepository;

    @Autowired
    private FinancialInfoMapper financialInfoMapper;

    @Autowired
    private FinancialInfoService financialInfoService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFinancialInfoMockMvc;

    private FinancialInfo financialInfo;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FinancialInfo createEntity(EntityManager em) {
        FinancialInfo financialInfo = new FinancialInfo()
            .monthlyNetIncome(DEFAULT_MONTHLY_NET_INCOME);
        return financialInfo;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FinancialInfo createUpdatedEntity(EntityManager em) {
        FinancialInfo financialInfo = new FinancialInfo()
            .monthlyNetIncome(UPDATED_MONTHLY_NET_INCOME);
        return financialInfo;
    }

    @BeforeEach
    public void initTest() {
        financialInfo = createEntity(em);
    }

    @Test
    @Transactional
    public void getAllFinancialInfos() throws Exception {
        // Initialize the database
        financialInfoRepository.saveAndFlush(financialInfo);

        // Get all the financialInfoList
        restFinancialInfoMockMvc.perform(get("/api/financial-infos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(financialInfo.getId().intValue())))
            .andExpect(jsonPath("$.[*].monthlyNetIncome").value(hasItem(DEFAULT_MONTHLY_NET_INCOME.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getFinancialInfo() throws Exception {
        // Initialize the database
        financialInfoRepository.saveAndFlush(financialInfo);

        // Get the financialInfo
        restFinancialInfoMockMvc.perform(get("/api/financial-infos/{id}", financialInfo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(financialInfo.getId().intValue()))
            .andExpect(jsonPath("$.monthlyNetIncome").value(DEFAULT_MONTHLY_NET_INCOME.doubleValue()));
    }
    @Test
    @Transactional
    public void getNonExistingFinancialInfo() throws Exception {
        // Get the financialInfo
        restFinancialInfoMockMvc.perform(get("/api/financial-infos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }
}
