package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.TfsRecetteApp;
import com.attijeri.tfs.domain.PersonalInfo;
import com.attijeri.tfs.repository.PersonalInfoRepository;
import com.attijeri.tfs.service.PersonalInfoService;
import com.attijeri.tfs.service.dto.PersonalInfoDTO;
import com.attijeri.tfs.service.mapper.PersonalInfoMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.attijeri.tfs.domain.enumeration.Civility;
/**
 * Integration tests for the {@link PersonalInfoResource} REST controller.
 */
@SpringBootTest(classes = TfsRecetteApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class PersonalInfoResourceIT {

    private static final Civility DEFAULT_CIVILITY = Civility.MADAME;
    private static final Civility UPDATED_CIVILITY = Civility.MONSIEUR;

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final Long DEFAULT_PHONE = 1L;
    private static final Long UPDATED_PHONE = 2L;

    private static final String DEFAULT_NATIVE_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_NATIVE_COUNTRY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_BIRTHDAY = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_BIRTHDAY = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_CLIENT_ABT = false;
    private static final Boolean UPDATED_CLIENT_ABT = true;

    private static final String DEFAULT_RIB = "AAAAAAAAAA";
    private static final String UPDATED_RIB = "BBBBBBBBBB";

    private static final String DEFAULT_NATIONALITY = "AAAAAAAAAA";
    private static final String UPDATED_NATIONALITY = "BBBBBBBBBB";

    private static final String DEFAULT_SECOND_NATIONALITY = "AAAAAAAAAA";
    private static final String UPDATED_SECOND_NATIONALITY = "BBBBBBBBBB";

    private static final String DEFAULT_MARITAL_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_MARITAL_STATUS = "BBBBBBBBBB";

    private static final Integer DEFAULT_NBR_KIDS = 1;
    private static final Integer UPDATED_NBR_KIDS = 2;

    private static final String DEFAULT_AMERICAN_INDEX = "AAAAAAAAAA";
    private static final String UPDATED_AMERICAN_INDEX = "BBBBBBBBBB";

    @Autowired
    private PersonalInfoRepository personalInfoRepository;

    @Autowired
    private PersonalInfoMapper personalInfoMapper;

    @Autowired
    private PersonalInfoService personalInfoService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPersonalInfoMockMvc;

    private PersonalInfo personalInfo;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PersonalInfo createEntity(EntityManager em) {
        PersonalInfo personalInfo = new PersonalInfo()
            .civility(DEFAULT_CIVILITY)
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .email(DEFAULT_EMAIL)
            .phone(DEFAULT_PHONE)
            .nativeCountry(DEFAULT_NATIVE_COUNTRY)
            .birthday(DEFAULT_BIRTHDAY)
            .clientABT(DEFAULT_CLIENT_ABT)
            .rib(DEFAULT_RIB)
            .nationality(DEFAULT_NATIONALITY)
            .secondNationality(DEFAULT_SECOND_NATIONALITY)
            .maritalStatus(DEFAULT_MARITAL_STATUS)
            .nbrKids(DEFAULT_NBR_KIDS)
            .americanIndex(DEFAULT_AMERICAN_INDEX);
        return personalInfo;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PersonalInfo createUpdatedEntity(EntityManager em) {
        PersonalInfo personalInfo = new PersonalInfo()
            .civility(UPDATED_CIVILITY)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .email(UPDATED_EMAIL)
            .phone(UPDATED_PHONE)
            .nativeCountry(UPDATED_NATIVE_COUNTRY)
            .birthday(UPDATED_BIRTHDAY)
            .clientABT(UPDATED_CLIENT_ABT)
            .rib(UPDATED_RIB)
            .nationality(UPDATED_NATIONALITY)
            .secondNationality(UPDATED_SECOND_NATIONALITY)
            .maritalStatus(UPDATED_MARITAL_STATUS)
            .nbrKids(UPDATED_NBR_KIDS)
            .americanIndex(UPDATED_AMERICAN_INDEX);
        return personalInfo;
    }

    @BeforeEach
    public void initTest() {
        personalInfo = createEntity(em);
    }

    @Test
    @Transactional
    public void getAllPersonalInfos() throws Exception {
        // Initialize the database
        personalInfoRepository.saveAndFlush(personalInfo);

        // Get all the personalInfoList
        restPersonalInfoMockMvc.perform(get("/api/personal-infos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(personalInfo.getId().intValue())))
            .andExpect(jsonPath("$.[*].civility").value(hasItem(DEFAULT_CIVILITY.toString())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.intValue())))
            .andExpect(jsonPath("$.[*].nativeCountry").value(hasItem(DEFAULT_NATIVE_COUNTRY)))
            .andExpect(jsonPath("$.[*].birthday").value(hasItem(DEFAULT_BIRTHDAY.toString())))
            .andExpect(jsonPath("$.[*].clientABT").value(hasItem(DEFAULT_CLIENT_ABT.booleanValue())))
            .andExpect(jsonPath("$.[*].rib").value(hasItem(DEFAULT_RIB)))
            .andExpect(jsonPath("$.[*].nationality").value(hasItem(DEFAULT_NATIONALITY)))
            .andExpect(jsonPath("$.[*].secondNationality").value(hasItem(DEFAULT_SECOND_NATIONALITY)))
            .andExpect(jsonPath("$.[*].maritalStatus").value(hasItem(DEFAULT_MARITAL_STATUS)))
            .andExpect(jsonPath("$.[*].nbrKids").value(hasItem(DEFAULT_NBR_KIDS)))
            .andExpect(jsonPath("$.[*].americanIndex").value(hasItem(DEFAULT_AMERICAN_INDEX)));
    }
    
    @Test
    @Transactional
    public void getPersonalInfo() throws Exception {
        // Initialize the database
        personalInfoRepository.saveAndFlush(personalInfo);

        // Get the personalInfo
        restPersonalInfoMockMvc.perform(get("/api/personal-infos/{id}", personalInfo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(personalInfo.getId().intValue()))
            .andExpect(jsonPath("$.civility").value(DEFAULT_CIVILITY.toString()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE.intValue()))
            .andExpect(jsonPath("$.nativeCountry").value(DEFAULT_NATIVE_COUNTRY))
            .andExpect(jsonPath("$.birthday").value(DEFAULT_BIRTHDAY.toString()))
            .andExpect(jsonPath("$.clientABT").value(DEFAULT_CLIENT_ABT.booleanValue()))
            .andExpect(jsonPath("$.rib").value(DEFAULT_RIB))
            .andExpect(jsonPath("$.nationality").value(DEFAULT_NATIONALITY))
            .andExpect(jsonPath("$.secondNationality").value(DEFAULT_SECOND_NATIONALITY))
            .andExpect(jsonPath("$.maritalStatus").value(DEFAULT_MARITAL_STATUS))
            .andExpect(jsonPath("$.nbrKids").value(DEFAULT_NBR_KIDS))
            .andExpect(jsonPath("$.americanIndex").value(DEFAULT_AMERICAN_INDEX));
    }
    @Test
    @Transactional
    public void getNonExistingPersonalInfo() throws Exception {
        // Get the personalInfo
        restPersonalInfoMockMvc.perform(get("/api/personal-infos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }
}
