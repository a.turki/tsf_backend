package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.TfsRecetteApp;
import com.attijeri.tfs.domain.RequiredDoc;
import com.attijeri.tfs.repository.RequiredDocRepository;
import com.attijeri.tfs.service.RequiredDocService;
import com.attijeri.tfs.service.dto.RequiredDocDTO;
import com.attijeri.tfs.service.mapper.RequiredDocMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RequiredDocResource} REST controller.
 */
@SpringBootTest(classes = TfsRecetteApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class RequiredDocResourceIT {

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_NUM_CIN = "AAAAAAAAAA";
    private static final String UPDATED_NUM_CIN = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DELIVERY_DATE_CIN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DELIVERY_DATE_CIN = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_RECTO_CIN = "AAAAAAAAAA";
    private static final String UPDATED_RECTO_CIN = "BBBBBBBBBB";

    private static final String DEFAULT_VERSO_CIN = "AAAAAAAAAA";
    private static final String UPDATED_VERSO_CIN = "BBBBBBBBBB";

    private static final String DEFAULT_FACTA = "AAAAAAAAAA";
    private static final String UPDATED_FACTA = "BBBBBBBBBB";

    @Autowired
    private RequiredDocRepository requiredDocRepository;

    @Autowired
    private RequiredDocMapper requiredDocMapper;

    @Autowired
    private RequiredDocService requiredDocService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restRequiredDocMockMvc;

    private RequiredDoc requiredDoc;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RequiredDoc createEntity(EntityManager em) {
        RequiredDoc requiredDoc = new RequiredDoc()
            .label(DEFAULT_LABEL)
            .type(DEFAULT_TYPE)
            .numCIN(DEFAULT_NUM_CIN)
            .deliveryDateCIN(DEFAULT_DELIVERY_DATE_CIN)
            .rectoCIN(DEFAULT_RECTO_CIN)
            .versoCIN(DEFAULT_VERSO_CIN)
            .facta(DEFAULT_FACTA);
        return requiredDoc;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RequiredDoc createUpdatedEntity(EntityManager em) {
        RequiredDoc requiredDoc = new RequiredDoc()
            .label(UPDATED_LABEL)
            .type(UPDATED_TYPE)
            .numCIN(UPDATED_NUM_CIN)
            .deliveryDateCIN(UPDATED_DELIVERY_DATE_CIN)
            .rectoCIN(UPDATED_RECTO_CIN)
            .versoCIN(UPDATED_VERSO_CIN)
            .facta(UPDATED_FACTA);
        return requiredDoc;
    }

    @BeforeEach
    public void initTest() {
        requiredDoc = createEntity(em);
    }

    @Test
    @Transactional
    public void getAllRequiredDocs() throws Exception {
        // Initialize the database
        requiredDocRepository.saveAndFlush(requiredDoc);

        // Get all the requiredDocList
        restRequiredDocMockMvc.perform(get("/api/required-docs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(requiredDoc.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)))
            .andExpect(jsonPath("$.[*].numCIN").value(hasItem(DEFAULT_NUM_CIN)))
            .andExpect(jsonPath("$.[*].deliveryDateCIN").value(hasItem(DEFAULT_DELIVERY_DATE_CIN.toString())))
            .andExpect(jsonPath("$.[*].rectoCIN").value(hasItem(DEFAULT_RECTO_CIN)))
            .andExpect(jsonPath("$.[*].versoCIN").value(hasItem(DEFAULT_VERSO_CIN)))
            .andExpect(jsonPath("$.[*].facta").value(hasItem(DEFAULT_FACTA)));
    }
    
    @Test
    @Transactional
    public void getRequiredDoc() throws Exception {
        // Initialize the database
        requiredDocRepository.saveAndFlush(requiredDoc);

        // Get the requiredDoc
        restRequiredDocMockMvc.perform(get("/api/required-docs/{id}", requiredDoc.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(requiredDoc.getId().intValue()))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE))
            .andExpect(jsonPath("$.numCIN").value(DEFAULT_NUM_CIN))
            .andExpect(jsonPath("$.deliveryDateCIN").value(DEFAULT_DELIVERY_DATE_CIN.toString()))
            .andExpect(jsonPath("$.rectoCIN").value(DEFAULT_RECTO_CIN))
            .andExpect(jsonPath("$.versoCIN").value(DEFAULT_VERSO_CIN))
            .andExpect(jsonPath("$.facta").value(DEFAULT_FACTA));
    }
    @Test
    @Transactional
    public void getNonExistingRequiredDoc() throws Exception {
        // Get the requiredDoc
        restRequiredDocMockMvc.perform(get("/api/required-docs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }
}
