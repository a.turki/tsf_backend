package com.attijeri.tfs.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.attijeri.tfs.web.rest.TestUtil;

public class RequiredDocIncomeDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RequiredDocIncomeDTO.class);
        RequiredDocIncomeDTO requiredDocIncomeDTO1 = new RequiredDocIncomeDTO();
        requiredDocIncomeDTO1.setId(1L);
        RequiredDocIncomeDTO requiredDocIncomeDTO2 = new RequiredDocIncomeDTO();
        assertThat(requiredDocIncomeDTO1).isNotEqualTo(requiredDocIncomeDTO2);
        requiredDocIncomeDTO2.setId(requiredDocIncomeDTO1.getId());
        assertThat(requiredDocIncomeDTO1).isEqualTo(requiredDocIncomeDTO2);
        requiredDocIncomeDTO2.setId(2L);
        assertThat(requiredDocIncomeDTO1).isNotEqualTo(requiredDocIncomeDTO2);
        requiredDocIncomeDTO1.setId(null);
        assertThat(requiredDocIncomeDTO1).isNotEqualTo(requiredDocIncomeDTO2);
    }
}
