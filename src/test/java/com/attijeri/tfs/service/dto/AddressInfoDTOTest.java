package com.attijeri.tfs.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.attijeri.tfs.web.rest.TestUtil;

public class AddressInfoDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AddressInfoDTO.class);
        AddressInfoDTO addressInfoDTO1 = new AddressInfoDTO();
        addressInfoDTO1.setId(1L);
        AddressInfoDTO addressInfoDTO2 = new AddressInfoDTO();
        assertThat(addressInfoDTO1).isNotEqualTo(addressInfoDTO2);
        addressInfoDTO2.setId(addressInfoDTO1.getId());
        assertThat(addressInfoDTO1).isEqualTo(addressInfoDTO2);
        addressInfoDTO2.setId(2L);
        assertThat(addressInfoDTO1).isNotEqualTo(addressInfoDTO2);
        addressInfoDTO1.setId(null);
        assertThat(addressInfoDTO1).isNotEqualTo(addressInfoDTO2);
    }
}
