package com.attijeri.tfs.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.attijeri.tfs.web.rest.TestUtil;

public class RequiredDocDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RequiredDocDTO.class);
        RequiredDocDTO requiredDocDTO1 = new RequiredDocDTO();
        requiredDocDTO1.setId(1L);
        RequiredDocDTO requiredDocDTO2 = new RequiredDocDTO();
        assertThat(requiredDocDTO1).isNotEqualTo(requiredDocDTO2);
        requiredDocDTO2.setId(requiredDocDTO1.getId());
        assertThat(requiredDocDTO1).isEqualTo(requiredDocDTO2);
        requiredDocDTO2.setId(2L);
        assertThat(requiredDocDTO1).isNotEqualTo(requiredDocDTO2);
        requiredDocDTO1.setId(null);
        assertThat(requiredDocDTO1).isNotEqualTo(requiredDocDTO2);
    }
}
