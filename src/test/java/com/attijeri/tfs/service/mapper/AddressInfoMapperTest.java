package com.attijeri.tfs.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class AddressInfoMapperTest {

    private AddressInfoMapper addressInfoMapper;

    @BeforeEach
    public void setUp() {
        addressInfoMapper = new AddressInfoMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(addressInfoMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(addressInfoMapper.fromId(null)).isNull();
    }
}
