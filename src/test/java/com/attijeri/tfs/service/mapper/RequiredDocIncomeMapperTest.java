package com.attijeri.tfs.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class RequiredDocIncomeMapperTest {

    private RequiredDocIncomeMapper requiredDocIncomeMapper;

    @BeforeEach
    public void setUp() {
        requiredDocIncomeMapper = new RequiredDocIncomeMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(requiredDocIncomeMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(requiredDocIncomeMapper.fromId(null)).isNull();
    }
}
