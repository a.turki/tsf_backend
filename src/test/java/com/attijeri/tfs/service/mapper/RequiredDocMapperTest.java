package com.attijeri.tfs.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class RequiredDocMapperTest {

    private RequiredDocMapper requiredDocMapper;

    @BeforeEach
    public void setUp() {
        requiredDocMapper = new RequiredDocMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(requiredDocMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(requiredDocMapper.fromId(null)).isNull();
    }
}
