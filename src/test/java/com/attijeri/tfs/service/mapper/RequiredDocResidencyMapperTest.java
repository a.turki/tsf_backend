package com.attijeri.tfs.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class RequiredDocResidencyMapperTest {

    private RequiredDocResidencyMapper requiredDocResidencyMapper;

    @BeforeEach
    public void setUp() {
        requiredDocResidencyMapper = new RequiredDocResidencyMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(requiredDocResidencyMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(requiredDocResidencyMapper.fromId(null)).isNull();
    }
}
